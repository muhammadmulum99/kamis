import React from "react";
import Headeruser from "../components/header/headeruser/Headeruser";
import Contentwishlistuser from "../components/contenthoteluser/Contentwishlistuser";
const Wishlist = () => {
  return (
    <>
      <Headeruser />
      <div style={{ marginBottom: "90px" }}>
        <Contentwishlistuser />
      </div>
      {/* <div>Wishlist</div> */}
    </>
  );
};

export default Wishlist;

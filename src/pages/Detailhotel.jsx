import React from "react";
import Headerhotels from "../components/header/headerhotels/Headerhotels";
import Contentdetailhotel from "../components/contenthotel/Contentdetailhotel";
const Detailhotel = () => {
  return (
    <>
      <Headerhotels />
      <Contentdetailhotel />
    </>
  );
};

export default Detailhotel;

import React from "react";
import Sidebarfo from "../components/sidebar/sidebarfo/Sidebarfo";
import Headerfo from "../components/header/headerfo/Headerfo";
import Contentcheckin from "../components/contentfo/checkin/Contentcheckin";
function Checkin() {
  return (
    <>
      <dev>
        {/* [ Pre-loader ] start */}
        <div class="loader-bg">
          <div class="loader-track">
            <div class="loader-fill"></div>
          </div>
        </div>
        {/* [ Pre-loader ] End */}
        {/* [ navigation menu ] start */}
        <Sidebarfo />
        {/* [ navigation menu ] end */}

        {/* [ Header ] start */}
        <Headerfo />
        {/* [ Header ] end */}

        {/* [ Main Content ] start */}
        <Contentcheckin />
        {/* [ Main Content ] start */}

        {/* [ Main Content ] end */}
        {/* [ Main Content ] end */}
      </dev>
    </>
  );
}

export default Checkin;

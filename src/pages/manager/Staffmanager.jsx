import React from "react";
import Managementstaff from "../../components/contentmanager/staff/Managementstaff";
import Headermanager from "../../components/header/headermanager/Headermanager";
import Sidebarmanager from "../../components/sidebar/sidebarmanagement/Sidebarmanager";

const Staffmanager = () => {
  return (
    <>
      <dev>
        {/* [ Pre-loader ] start */}
        <div class="loader-bg">
          <div class="loader-track">
            <div class="loader-fill"></div>
          </div>
        </div>
        {/* [ Pre-loader ] End */}
        {/* [ navigation menu ] start */}
        <Sidebarmanager />
        {/* [ navigation menu ] end */}

        {/* [ Header ] start */}
        <Headermanager />
        {/* [ Header ] end */}

        {/* [ Main Content ] start */}
        <Managementstaff />
        {/* [ Main Content ] start */}

        {/* [ Main Content ] end */}
        {/* [ Main Content ] end */}
      </dev>
    </>
  );
};

export default Staffmanager;

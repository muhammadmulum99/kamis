import React from "react";
import Sidebarfo from "../components/sidebar/sidebarfo/Sidebarfo";
import Headerfo from "../components/header/headerfo/Headerfo";
// import Contentorderroom from "../components/contentfo/roomorder/Contentorderroom";
import Contentkasir from "../components/contentfo/kasir/Contentkasir";
function Kasir() {
  return (
    <>
      {/* [ Pre-loader ] start */}
      <div class="loader-bg">
        <div class="loader-track">
          <div class="loader-fill"></div>
        </div>
      </div>
      {/* [ Pre-loader ] End */}
      {/* [ navigation menu ] start */}
      <Sidebarfo />
      {/* [ navigation menu ] end */}

      {/* [ Header ] start */}
      <Headerfo />
      {/* [ Header ] end */}

      {/* [ Main Content ] start */}

      <Contentkasir />

      {/* [ Main Content ] end */}
    </>
  );
}

export default Kasir;

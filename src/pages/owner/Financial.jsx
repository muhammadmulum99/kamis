import React from "react";
import Sidebarowner from "../../components/sidebar/sidebarowner/Sidebarowner";
import Headerowner from "../../components/header/headerowner/Headerowner";
import Financialcontent from "../../components/contentowner/financial/Financialcontent";

function Financial() {
  return (
    <>
      <dev>
        {/* [ Pre-loader ] start */}
        <div class="loader-bg">
          <div class="loader-track">
            <div class="loader-fill"></div>
          </div>
        </div>
        {/* [ Pre-loader ] End */}
        {/* [ navigation menu ] start */}
        <Sidebarowner />
        {/* [ navigation menu ] end */}

        {/* [ Header ] start */}
        <Headerowner />
        {/* [ Header ] end */}

        {/* [ Main Content ] start */}
        <Financialcontent />
        {/* [ Main Content ] end */}
      </dev>
    </>
  );
}

export default Financial;
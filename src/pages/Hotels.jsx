import React from "react";
import Headerhotels from "../components/header/headerhotels/Headerhotels";
import Contenthoteluser from "../components/contenthoteluser/Contenthoteluser";
import { Container } from "react-bootstrap";

const Hotels = () => {
  return (
    <>
      <Headerhotels />
      <Contenthoteluser />
    </>
  );
};

export default Hotels;

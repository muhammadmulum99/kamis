import React from "react";
import Sidebarfo from "../components/sidebar/sidebarfo/Sidebarfo";
import Headerfo from "../components/header/headerfo/Headerfo";
import Contentcheckout from "../components/contentfo/checkout/Contentcheckout";

function Checkout() {
  return (
    <dev>
      {/* [ Pre-loader ] start */}
      <div class="loader-bg">
        <div class="loader-track">
          <div class="loader-fill"></div>
        </div>
      </div>
      {/* [ Pre-loader ] End */}
      {/* [ navigation menu ] start */}
      <Sidebarfo />
      {/* [ navigation menu ] end */}

      {/* [ Header ] start */}
      <Headerfo />
      {/* [ Header ] end */}

      {/* [ Main Content ] start */}
      <Contentcheckout />
      {/* [ Main Content ] end */}
    </dev>
  );
}

export default Checkout;

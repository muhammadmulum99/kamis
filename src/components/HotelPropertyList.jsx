import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import cryptoJs from "crypto-js";

const Banklist = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  const getUsers = async () => {
    const response = await axios.get("http://localhost:5000/propertihotel");
    console.log(response);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
    setUsers(decryptedData);
  };

  const deleteUser = async (userId) => {
    await axios.delete(`http://localhost:5000/propertihotel/${userId}`);
    getUsers();
  };

  const downloadBase64File = (base64Data, x) => {
    const linkSource = `data:image/jpeg;base64,${base64Data}`;
    const downloadLink = document.createElement("a");
    downloadLink.href = linkSource;
    downloadLink.download = x+'.jpeg';
    downloadLink.click();
  }

  return (
    <div>
      <h1 className="title">Hotel Property</h1>
      <h2 className="subtitle">List of Hotel Property</h2>
      <Link to="/hotelproperty/add" className="button is-primary mb-2">
        Add New
      </Link>
      <table className="table is-striped is-fullwidth">
        <thead>
          <tr>
            <th>No</th>
            <th>Id Property Hotel</th>
            <th>Jenis Hotel</th>
            <th>Alamat Hotel</th>
            <th>Provinsi Hotel</th>
            <th>Kecamatan Hotel</th>
            <th>Long Lat Hotel</th>
            <th>Id owner</th>
            <th>Foto Hotel</th>
            <th>Fasilitas</th>

            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user, index) => (
            <tr key={user.uuid}>
              <td>{index + 1}</td>
              <td>{user.uuid_properti_hotel}</td>
              <td>{user.jenis_hotel}</td>
              <td>{user.alamat_hotel}</td>
              <td>{user.provinsi_hotel}</td>
              <td>{user.kecamatan_hotel}</td>
              <td>{user.long_lat_hotel}</td>
              <td>{user.id_owner}</td>
              <td><button onClick={() => downloadBase64File(user.foto_hotel, user.jenis_hotel)}>Download Photo</button></td>
              <td>{user.fasilitas}</td>
              <td>
                <Link
                  to={`/hotelproperty/edit/${user.uuid_properti_hotel}`}
                  className="button is-small is-info"
                >
                  Edit
                </Link>
                <button
                  onClick={() => deleteUser(user.uuid_properti_hotel)}
                  className="button is-small is-danger"
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Banklist;

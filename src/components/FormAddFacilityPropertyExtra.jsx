import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const FormAddBank = () => {
  const initialState = {
    uuid_properti_fasilitas_ekstra: '',
    id_properti_hotel: '',
    id_properti_kamar: '',
    id_master_fasilitas: '',
    status_properti_fasilitas_ekstra: '',
    foto_fasilitas_ekstra: '',
  }
  const [data, setData] = useState(initialState);
  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  const [msg, setMsg] = useState("");
  const navigate = useNavigate();

  const saveUser = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/propertifasilitasekstra", data);
      navigate("/facilitypropertyextra");
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  };
  const uploadFiles = (e) => {
    var f = e.target.files[0]; // FileList object
    var reader = new FileReader();
    // Closure to capture the file information.
    reader.onload = (function(theFile) {
      return function(e) {
        var binaryData = e.target.result;
        //Converting Binary Data to base 64
        var base64String = window.btoa(binaryData);
        //showing file converted to base64
        // document.getElementById('base64').value = base64String;
        setData({
          ...data,
          foto_fasilitas_ekstra: base64String,
        })
        // alert('File converted to base64 successfuly!\nCheck in Textarea');
      };
    })(f);
    // Read in the image file as a data URL.
    reader.readAsBinaryString(f);
  }
  return (
    <div>
      <h1 className="title">Property Facility Extra</h1>
      <h2 className="subtitle">Add New Property Facility Extra</h2>
      <div className="card is-shadowless">
        <div className="card-content">
          <div className="content">
            <form onSubmit={saveUser}>
              <p className="has-text-centered">{msg}</p>
              <div className="field">
                <label className="label">ID Property Fasilitas Extra</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="uuid_properti_fasilitas_ekstra"
                    value={data.uuid_properti_fasilitas_ekstra}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">ID Property Hotel</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="id_properti_hotel"
                    value={data.id_properti_hotel}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">ID Property Room</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="id_properti_kamar"
                    value={data.id_properti_kamar}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">ID Master Facility</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="id_master_fasilitas"
                    value={data.id_master_fasilitas}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Status</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="status_properti_fasilitas_ekstra"
                    value={data.status_properti_fasilitas_ekstra}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Photo</label>
                <div className="control">
                  <input
                    type="file"
                    className="input"
                    name="foto_fasilitas_ekstra"
                    onChange={uploadFiles}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <button type="submit" className="button is-success">
                    Save
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormAddBank;

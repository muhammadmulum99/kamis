import { useNavigate } from "react-router-dom";
import "./searchitem.css";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import axios from "axios";
import { getMe } from "../../features/authSlice";
const SearchItem = ({ item }) => {
  let today = new Date();
  
  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);
  const navigate = useNavigate();
  const handledetail = () => {
    navigate("/hotels/"+item.id_properti_hotel);
    localStorage.setItem('detail',JSON.stringify(data))
  };
  const initialState = {
    ...item,
    tgl_checkin: '',
    tgl_checkout: '',
    total_biaya: '',
    jumlah_kamar: '',
    id_user: ""
  }
  const [show, setShow] = useState(false)
  const [data, setData] = useState(initialState)
  
  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }

  const handleHarga = (e) => {
    let date = new Date(data.tgl_checkin)
    if(date.getDay() > 4){
      setData({
        ...data,
        total_biaya: (parseInt(item.harga_weekend) * parseInt(data.jumlah_kamar)).toString()
      })
    }
    else{
      setData({
        ...data,
        total_biaya: (parseInt(item.harga_weekday) * parseInt(data.jumlah_kamar)).toString()
      })
    }
  }

  useEffect(() => {
    if(data.jumlah_kamar === '') return;
    handleHarga()
  }, [data.jumlah_kamar]);

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      // navigate("/");
    }
    if(user){
      setData({...data, id_user: user.uuid})
    }
  }, [isError, user, navigate]);

  const handleWishlist = async () => {
    try {
      await axios.post("http://localhost:5000/keranjang", data);
      navigate("/wishlist");
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
        navigate("/login");
      }
    }
  }


  return (
    <div className="searchItem">
      <img
        src={item.foto_kamar ? "data:image/jpeg;base64," + item.foto_kamar : "https://cf.bstatic.com/xdata/images/hotel/square600/261707778.webp?k=fa6b6128468ec15e81f7d076b6f2473fa3a80c255582f155cae35f9edbffdd78&o=&s=1"}
        alt=""
        className="siImg"
      />
      <div className="siDesc">
        <h1 className="siTitle">{item.jenis_hotel}</h1>
        <span className="siDistance">500m from center</span>
        {/* <span className="siTaxiOp">Free airport taxi</span> */}
        <span className="siSubtitle">
          {item.type_kamar}
        </span>
        <span className="siFeatures">
          {item.fasilitas_kamar}
        </span>
        {/* <span className="siCancelOp">Free cancellation </span>
        <span className="siCancelOpSubtitle">
          You can cancel later, so lock in this great price today!
        </span> */}
      </div>
      <div className="siDetails">
        <div className="siRating">
          <span>Excellent</span>
          <button>{item.rating}</button>
        </div>
        <div className="siDetailTexts">
          <span className="siPrice">Rp.{today.getDay() > 4 ? item.harga_weekend : item.harga_weekday}</span>
          <span className="siTaxOp">Includes taxes and fees</span>
          <button className="siCheckButton" onClick={handledetail}>
            See availability
          </button>
          <button className="siCheckButton" onClick={() => setShow(true)}>
            Wishlist
          </button>
        </div>
      </div>
      {show ?
        <div className="siDesc">
          <input name="tgl_checkin" type="date" onChange={handleInputChange}/>
          <input name="tgl_checkout" type="date" onChange={handleInputChange}/>
          <input name="jumlah_kamar" type="number" onChange={handleInputChange}/>
          <button className="siCheckButton" onClick={handleWishlist}>
            Add to Wishlist
          </button>
        </div> : null    
      }
    </div>
  );
};

export default SearchItem;

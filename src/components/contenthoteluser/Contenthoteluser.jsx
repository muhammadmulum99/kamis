import React from "react";
import { Container, Row, Col, Form, Button, Card } from "react-bootstrap";
import "./contenthoteluser.css";
import SearchItem from "./Wishlistitem";

const Contenthoteluser = () => {
  let hotels = localStorage.getItem('hotels') ? JSON.parse(localStorage.getItem('hotels')) : []
  function valueLabelFormat(value) {
    const units = ["rupiah"];

    let unitIndex = 0;
    let scaledValue = value;

    while (scaledValue >= 50000 && unitIndex < units.length - 1) {
      unitIndex += 1;
      scaledValue *= 10;
    }

    return `${scaledValue} ${units[unitIndex]}`;
  }

  function calculateValue(value) {
    return value;
  }

  const [value, setValue] = React.useState(10);

  const handleChange = (event, newValue) => {
    if (typeof newValue === "number") {
      setValue(newValue);
    }
  };

  return (
    <>
      <div className="main-content-hotels">
        <Container>
          {/* Stack the columns on mobile by making one full-width and the other half-width */}
          <Row>
            <Col xs={6} md={4} style={{}}>
              <Card>
                <Card.Body>
                  <Form>
                    <Form.Group className="mb-3" controlId="formBasicEmail">

                      <Form.Label>Payment</Form.Label>
                      <Form.Check
                        aria-label="option 1"
                        label="Payment on Check in"
                        name="group1"
                      />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Check
                        aria-label="option 1"
                        label="Down-Payment"
                        name="group1"
                      />
                    </Form.Group>
                    <Form.Label>Facility</Form.Label>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Check
                        aria-label="option 1"
                        label="Breakfast"
                        name="group1"
                      />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Check
                        aria-label="option 1"
                        label="Gym"
                        name="group1"
                      />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Check
                        aria-label="option 1"
                        label="Swiming Pool"
                        name="group1"
                      />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Check
                        aria-label="option 1"
                        label="Bar"
                        name="group1"
                      />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Check
                        aria-label="option 1"
                        label="Water Heater"
                        name="group1"
                      />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Check
                        aria-label="option 1"
                        label="Park"
                        name="group1"
                      />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Check
                        aria-label="option 1"
                        label="Sports"
                        name="group1"
                      />
                    </Form.Group>
                    <Button variant="warning" onClick={() => console.log(hotels)} size="lg">
                      Submit
                    </Button>
                  </Form>
                </Card.Body>
              </Card>
            </Col>

            <Col xs={12} md={8} style={{}}>
              <Card>
                <Card.Body>
                  {hotels.map((item,key) => {
                    return(
                      <SearchItem item={item}/>    
                    )
                  })}
                </Card.Body>
              </Card>
            </Col>
          </Row>

          {/* Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop */}

          {/* Columns are always 50% wide, on mobile and desktop */}
        </Container>
      </div>
    </>
  );
};

export default Contenthoteluser;

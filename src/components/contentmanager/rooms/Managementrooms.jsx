import React from "react";
import { useState, useEffect } from "react";
import { Button, Table, Dropdown, Image, Card, Modal, Form, Row, Col } from "react-bootstrap";
import florplan from "./florplan.jpg";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { getMe } from "../../../features/authSlice";
import cryptoJs from "crypto-js";

const Managementrooms = () => {
  const [modal, setModal] = useState(false);
  const [keys, setKeys] = useState(0)

  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);
  const navigate = useNavigate();

  const handleClose = () => setModal(false);
  const handleShow = () => setModal(true);

  const dummy = [
    {
      id_properti_hotel: '6',
      facility: 'Room 101',
      equipment_name: '',
      status_facility: 'Open',
      total_equipment: '',
      floor_number: '1',
      open_hours: '24',
      equipment_condition: '1',
    },
    {
      id_properti_hotel: '6',
      facility: 'Room 101',
      equipment_name: 'Single Bed',
      status_facility: '1',
      total_equipment: '1',
      floor_number: '1',
      open_hours: '24',
      equipment_condition: '1',
    },
    {
      id_properti_hotel: '6',
      facility: 'Room 101',
      equipment_name: 'Television',
      status_facility: '1',
      total_equipment: '1',
      floor_number: '1',
      open_hours: '24',
      equipment_condition: '1',
    },
    {
      id_properti_hotel: '6',
      facility: 'Room 101',
      equipment_name: 'Balcony',
      status_facility: '1',
      total_equipment: '3',
      floor_number: '1',
      open_hours: '24',
      equipment_condition: '1',
    },
    {
      id_properti_hotel: '6',
      facility: 'Room 101',
      equipment_name: 'Mini Fridge',
      status_facility: '1',
      total_equipment: '2',
      floor_number: '1',
      open_hours: '24',
      equipment_condition: '1',
    },
    {
      id_properti_hotel: '6',
      facility: 'Room 101',
      equipment_name: 'Bathtub',
      status_facility: '1',
      total_equipment: '1',
      floor_number: '1',
      open_hours: '24',
      equipment_condition: '1',
    },
    {
      id_properti_hotel: '6',
      facility: 'Room 101',
      equipment_name: 'Shower',
      status_facility: '1',
      total_equipment: '1',
      floor_number: '1',
      open_hours: '24',
      equipment_condition: '1',
    },
    {
      id_properti_hotel: '6',
      facility: 'Room 101',
      equipment_name: 'Water Heater',
      status_facility: '1',
      total_equipment: '3',
      floor_number: '1',
      open_hours: '24',
      equipment_condition: '1',
    },
    {
      id_properti_hotel: '6',
      facility: 'Room 101',
      equipment_name: 'Sink',
      status_facility: '1',
      total_equipment: '2',
      floor_number: '1',
      open_hours: '24',
      equipment_condition: '1',
    },
    {
      id_properti_hotel: '6',
      facility: 'Room 102',
      equipment_name: '',
      status_facility: 'Open',
      total_equipment: '',
      floor_number: '1',
      open_hours: '24',
      equipment_condition: '1',
    },
    {
      id_properti_hotel: '6',
      facility: 'Room 102',
      equipment_name: 'Single Bed',
      status_facility: '1',
      total_equipment: '1',
      floor_number: '1',
      open_hours: '24',
      equipment_condition: '1',
    },
    {
      id_properti_hotel: '6',
      facility: 'Room 102',
      equipment_name: 'Television',
      status_facility: '1',
      total_equipment: '1',
      floor_number: '1',
      open_hours: '24',
      equipment_condition: '1',
    },
    {
      id_properti_hotel: '6',
      facility: 'Room 102',
      equipment_name: 'Balcony',
      status_facility: '1',
      total_equipment: '3',
      floor_number: '1',
      open_hours: '24',
      equipment_condition: '1',
    },
    {
      id_properti_hotel: '6',
      facility: 'Room 102',
      equipment_name: 'Mini Fridge',
      status_facility: '1',
      total_equipment: '2',
      floor_number: '1',
      open_hours: '24',
      equipment_condition: '1',
    },
    {
      id_properti_hotel: '6',
      facility: 'Room 102',
      equipment_name: 'Bathtub',
      status_facility: '1',
      total_equipment: '1',
      floor_number: '1',
      open_hours: '24',
      equipment_condition: '1',
    },
    {
      id_properti_hotel: '6',
      facility: 'Room 102',
      equipment_name: 'Shower',
      status_facility: '1',
      total_equipment: '1',
      floor_number: '1',
      open_hours: '24',
      equipment_condition: '1',
    },
    {
      id_properti_hotel: '6',
      facility: 'Room 102',
      equipment_name: 'Water Heater',
      status_facility: '1',
      total_equipment: '3',
      floor_number: '1',
      open_hours: '24',
      equipment_condition: '1',
    },
    {
      id_properti_hotel: '6',
      facility: 'Room 102',
      equipment_name: 'Sink',
      status_facility: '1',
      total_equipment: '2',
      floor_number: '1',
      open_hours: '24',
      equipment_condition: '1',
    },
  ]

  const initialState = {
    // uuid_staff: "",
    id: "",
    nama_staff: "",
    email: "",
    phone_number: "",
    position: "",
    username: "",
    password: "",
    id_owner: "",
    id_management: "",
    id_properti_hotel: "",
    address: "",
  }

  const [data, setData] = useState(initialState)
  const [staff, setStaff] = useState([])

  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  
  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      // navigate("/");
    }
    if(user){
      setData({...data, id_management: user.uuid})
    }
  }, [isError, user, navigate]);

   const getHotel = async () => {
    const response = await axios.get(`http://localhost:5000/managementhotel/${data.id_management}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setData({
        ...data,
        id_properti_hotel: decryptedData[0].uuid_properti_hotel
      })
    }    
  }

  const getStaff = async () => {
    const response = await axios.get(`http://localhost:5000/managementstaff/${data.id_management}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setStaff(decryptedData)
    }    
  }

  useEffect(() => {
    if(data.id_management === '') return;
    getHotel();
    getStaff();
  },[data.id_management])

  const saveStaff = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/createstaffmanagement", data);
      getStaff();
      setData(initialState)
      handleClose();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const updateStaff = async (e) => {
    e.preventDefault();
    try {
      await axios.patch("http://localhost:5000/updatestaffmanagement/"+data.id, data);
      getStaff();
      setData(initialState);
      handleClose();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const setInputFromTable = (key) => {
    setData({
      ...data,
      id: staff[key].id,
      nama_staff: staff[key].nama_staff,
      email: staff[key].email,
      phone_number: staff[key].phone_number,
      position: staff[key].position,
      username: staff[key].username,
      password: staff[key].password,
      id_owner: staff[key].id_owner,
      id_management: staff[key].id_management,
      id_properti_hotel: staff[key].id_properti_hotel,
    })
  }

  return (
    <>
      {/* [ Main Content ] start */}
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              {/* [ breadcrumb ] start */}
              <div class="page-header">
                <div class="page-block">
                  <div class="row align-items-center">
                    <div class="col-md-12">
                      <div class="page-header-title">
                        <h5 class="m-b-10">Typography</h5>
                      </div>
                      <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="index.html">
                            <i class="feather icon-home"></i>
                          </a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Basic Componants</a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Typography</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/* [ breadcrumb ] end */}
              <div class="main-body">
                <div class="page-wrapper">
                  {/* [ Main Content ] start */}
                  <div class="row">
                    {/* [ Typography ] start */}
                    <div class="col-sm-8">
                      <div class="card">
                        <div class="card-header">
                          <h5>Hotel Floor Plan</h5>
                          <div style={{ float: "right" }}>
                            <Dropdown>
                              <Dropdown.Toggle
                                variant="light"
                                id="dropdown-basic"
                              >
                                Floor
                              </Dropdown.Toggle>

                              <Dropdown.Menu>
                                <Dropdown.Item href="#/action-1">
                                  Lantai 1
                                </Dropdown.Item>
                                <Dropdown.Item href="#/action-2">
                                  Laintai 2
                                </Dropdown.Item>
                                <Dropdown.Item href="#/action-3">
                                  Lantai 3
                                </Dropdown.Item>
                              </Dropdown.Menu>
                            </Dropdown>
                          </div>
                        </div>
                        <div class="card-body">
                          <Card.Img variant="top" src={florplan} />
                        </div>
                      </div>
                    </div>

                    {/* test */}
                    <div class="col-sm-4">
                      <div class="card">
                        <div class="card-header">
                          <h5>Rooms</h5>

                          <Button
                            style={{
                              float: "right",
                              backgroundColor: "#8F00FF",
                            }}
                          >
                            Edit
                          </Button>
                        </div>
                        <div class="card-body">
                          {dummy.map((item,key) => {
                            return(
                              item.total_equipment === '' ?
                              <h5>{item.facility}: {item.status_facility}</h5> :
                              <div className="contentkiri-promo">
                                <h3>{item.equipment_name}:</h3>
                                <p>{item.total_equipment}</p>
                                <Button
                                  style={{
                                    float: "right",
                                    backgroundColor: "#8F00FF",
                                  }}
                                  onClick={() => {
                                    setModal(true)
                                    setKeys(key)
                                  }}
                                >
                                  Edit
                                </Button>
                              </div>
                            )
                          })}
                          <hr align="left" width="350" />
                        </div>
                      </div>
                    </div>
                    {/* [ Typography ] end */}
                  </div>
                  {/* [ Main Content ] end */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal show={modal} onHide={handleClose} size='lg'>
        <Modal.Header closeButton>
          <Modal.Title>Add/edit facility</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
          <Row className="mb-2">
              <Col xs='auto'>
                <Form.Control as="select" value={keys === 0 ? '' : dummy[keys].floor_number}>
                  <option value="">Floor Level</option>
                  <option value="1">Floor 1</option>
                  <option value="2">Floor 2</option>
                  <option value="3">Floor 3</option>
                </Form.Control>
              </Col>
              <Col>
                <Form.Control type="file" placeholder="Upload Floor Layout Picture"/>
              </Col>
            </Row>
            <Row className="mb-2">
              <Col xs='auto'>
                <Form.Control type="text" placeholder="Facility Name" value={keys === 0 ? '' : dummy[keys].facility}/>
              </Col>
              <Col>
                <Form.Control type="text" placeholder="Open Hours"value={keys === 0 ? '' : dummy[keys].open_hours}/>
              </Col>
            </Row>
            <Row className="mb-2">
              <Col xs='auto'>
                <Form.Control type="text" placeholder="Equipment Name" value={keys === 0 ? '' : dummy[keys].equipment_name}/>
              </Col>
              <Col>
                <Form.Control type="text" placeholder="Equipment Count" value={keys === 0 ? '' : dummy[keys].total_equipment}/>
              </Col>
              <Col>
              <Form.Control as="select" value={keys === 0 ? '' : dummy[keys].status_facility}>
                  <option value="">Equipment Condition</option>
                  <option value="1">Active</option>
                  <option value="2">Inactive</option>
                  <option value="3">Under Maintainance</option>
                </Form.Control>
              </Col>
            </Row>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal> 
      {/* [ Main Content ] end */}
    </>
  );
};

export default Managementrooms;

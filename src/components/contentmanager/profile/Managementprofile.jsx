import React from "react";
import { Table } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import axios from "axios";
import { getMe } from "../../../features/authSlice";
import cryptoJs from "crypto-js";

const Managementprofile = () => {
  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);
  const navigate = useNavigate();

  const initialState = {
    id_user: ""
  }

  const [data, setData] = useState(initialState)
  const [management, setManagement] = useState({})
  const [facility, setFacility] = useState([])
  const [room, setRoom] = useState([])
  const [staff, setStaff] = useState([])

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      // navigate("/");
    }
    if(user){
      setData({...data, id_user: user.uuid})
    }
  }, [isError, user, navigate]);

  const getProfile = async () => {
    const response = await axios.get(`http://localhost:5000/getprofilemanagement/${data.id_user}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setManagement(decryptedData.datamanagement[0])
      setFacility(decryptedData.datafasilitas)
      setRoom(decryptedData.dataroom)
      setStaff(decryptedData.datastaff)
    }    
  }

  useEffect(() => {
    if(data.id_user === '') return;
    getProfile();
  },[data.id_user])

  return (
    <>
      {/* [ Main Content ] start */}
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              {/* [ breadcrumb ] start */}
              <div class="page-header">
                <div class="page-block">
                  <div class="row align-items-center">
                    <div class="col-md-12">
                      <div class="page-header-title">
                        <h5 class="m-b-10">Typography</h5>
                      </div>
                      <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="index.html">
                            <i class="feather icon-home"></i>
                          </a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Basic Componants</a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Typography</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/* [ breadcrumb ] end */}
              <div class="main-body">
                <div class="page-wrapper">
                  {/* [ Main Content ] start */}
                  <div class="row">
                    {/* [ Typography ] start */}
                    <div class="col-sm-12">
                      <div class="card">
                        <div class="card-header">
                          <h5>Profile</h5>
                        </div>
                        <div class="card-body">
                          <h5>{management ? management.name : 'Management Name'}</h5>
                          <p>{management ? management.jenis_hotel : 'Property Name'}e</p>
                          <p>{management ? management.alamat_hotel : 'Property Address'}</p>
                          <h6>Facilities</h6>
                          <div class="col-sm-6">
                            <Table striped bordered hover>
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Name Facility</th>
                                  <th>Status Facilities</th>
                                </tr>
                              </thead>
                              <tbody>
                                {facility.filter((fil) => fil.id_properti_kamar === '' && fil.status_properti_fasilitas_ekstra !== '').map((item,key) => {
                                  return(
                                    <tr>
                                      <td>{key+1}</td>
                                      <td>{item.nama_master_fasilitas}</td>
                                      <td>{item.status_properti_fasilitas_ekstra}</td>
                                    </tr>
                                  )
                                })}
                              </tbody>
                            </Table>
                          </div>

                          <h6>Rooms</h6>
                          {room.map((item,key) => {
                            return(
                              key === 0 ?
                              <div class="row">
                                <div class="col-sm-4">
                                  <Table striped bordered hover>
                                    <thead>
                                      <tr>
                                        <th></th>
                                        <th>{room[key].type_kamar}</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Total Rooms</td>
                                        <td>{room[key].total_status}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Rooms Under Maintenence</td>
                                        <td>0</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </div>
                                {room[key+1] ?
                                <div class="col-sm-4">
                                  <Table striped bordered hover>
                                    <thead>
                                      <tr>
                                        <th></th>
                                        <th>{room[key+1].type_kamar}</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Total Rooms</td>
                                        <td>{room[key+1].total_status}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Rooms Under Maintenence</td>
                                        <td>0</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </div> :
                                <div class="col-sm-4"></div>}
                                {room[key+2] ?
                                <div class="col-sm-4">
                                  <Table striped bordered hover>
                                    <thead>
                                      <tr>
                                        <th></th>
                                        <th>{room[key+2].type_kamar}</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Total Rooms</td>
                                        <td>{room[key+2].total_status}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Rooms Under Maintenence</td>
                                        <td>0</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </div> :
                                <div class="col-sm-4"></div>}
                              </div> :
                              key === room.length - 2 ?
                              null :
                              <div class="row">
                                {room[key*3] ?
                                <div class="col-sm-4">
                                  <Table striped bordered hover>
                                    <thead>
                                      <tr>
                                        <th></th>
                                        <th>{room[key*3].type_kamar}</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Total Rooms</td>
                                        <td>{room[key*3].total_status}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Rooms Under Maintenence</td>
                                        <td>0</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </div> :
                                <div class="col-sm-4"></div>}
                                {room[key*3+1] ?
                                <div class="col-sm-4">
                                  <Table striped bordered hover>
                                    <thead>
                                      <tr>
                                        <th></th>
                                        <th>{room[key*3+1].type_kamar}</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Total Rooms</td>
                                        <td>{room[key*3+1].total_status}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Rooms Under Maintenence</td>
                                        <td>0</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </div> :
                                <div class="col-sm-4"></div>}
                                {room[key*3+2] ?
                                <div class="col-sm-4">
                                  <Table striped bordered hover>
                                    <thead>
                                      <tr>
                                        <th></th>
                                        <th>{room[key*3+2].type_kamar}</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Total Rooms</td>
                                        <td>{room[key+2].total_status}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Rooms Under Maintenence</td>
                                        <td>0</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </div> :
                                <div class="col-sm-4"></div>}
                              </div>
                            )
                          })}
                          

                          <h6>Staff</h6>
                          <div class="col-sm-6">
                            <Table striped bordered hover>
                              <thead>
                                <tr>
                                  <th>Status Staff</th>
                                  <th>Jumlah</th>
                                </tr>
                              </thead>
                              <tbody>
                                {staff.map((item,key) => {
                                  return(
                                    <tr>
                                      <td>Total Staff</td>
                                      <td>{item.total}</td>
                                    </tr>
                                  )
                                })}
                              </tbody>
                            </Table>
                          </div>

                          <h6>Last Financial Update : 2022/10/14</h6>
                          <h6>Last Report Update : 2022/10/14</h6>
                        </div>
                      </div>
                    </div>

                    {/* [ Typography ] end */}
                  </div>
                  {/* [ Main Content ] end */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* [ Main Content ] end */}
    </>
  );
};

export default Managementprofile;

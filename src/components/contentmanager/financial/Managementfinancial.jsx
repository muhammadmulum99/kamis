import React from "react";
import { useState } from "react";
import { Button, Table, Dropdown, Image, Card, Modal, Form, Row, Col } from "react-bootstrap";
import { BsInfoCircle } from "react-icons/bs";

const Managementfinancial = () => {
  const [modal, setModal] = useState(false);
  const [keys, setKeys] = useState(0)

  const handleClose = () => {
    setModal(false)
  }

  const dummy = [
    {
      time: '10:00',
      date: '2022-10-13',
      income: '2000000',
      costs: '100000',
      profit: '3000000'
    },
  ]
  return (
    <>
      {/* [ Main Content ] start */}
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              {/* [ breadcrumb ] start */}
              <div class="page-header">
                <div class="page-block">
                  <div class="row align-items-center">
                    <div class="col-md-12">
                      <div class="page-header-title">
                        <h5 class="m-b-10">Typography</h5>
                      </div>
                      <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="index.html">
                            <i class="feather icon-home"></i>
                          </a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Basic Componants</a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Typography</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/* [ breadcrumb ] end */}
              <div class="main-body">
                <div class="page-wrapper">
                  {/* [ Main Content ] start */}
                  <div class="row">
                    {/* [ Typography ] start */}
                    <div class="col-sm-12">
                      <div class="card">
                        <div class="card-header">
                          <h5>Financial</h5>
                          <Button onClick={() => {
                              setModal(true)
                              setKeys(-1)
                            }} style={{ float: "right" }}>Add</Button>
                        </div>
                        <div class="card-body">
                          <Table striped bordered hover>
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Time</th>
                                <th>Date</th>
                                <th>Income</th>
                                <th>Conts</th>
                                <th>Profit</th>
                                <th>Details</th>
                              </tr>
                            </thead>
                            <tbody>
                              
                              {dummy.map((item,key) => {
                                return(
                                  <tr>
                                    <td>{key}</td>
                                    <td>{item.time}</td>
                                    <td>{item.date}</td>
                                    <td>{item.income}</td>
                                    <td>{item.costs}</td>
                                    <td>{item.profit}</td>
                                    <td>
                                      <Button 
                                      onClick={() => {
                                        setModal(true)
                                        setKeys(key)
                                      }}>
                                        Edit
                                      </Button>
                                      <Button>
                                        Download
                                      </Button>
                                    </td>
                                  </tr>
                                )
                              })}
                            </tbody>
                          </Table>
                        </div>
                      </div>
                    </div>

                    {/* [ Typography ] end */}
                  </div>
                  {/* [ Main Content ] end */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal show={modal} onHide={handleClose} size='lg'>
        <Modal.Header closeButton>
          <Modal.Title>Add/edit financial report</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
          <Row className="mb-2">
              <Col xs='auto'>
                <Form.Control type="time" value={keys === -1 ? '' : dummy[keys].time}>
                </Form.Control>
              </Col>
              <Col>
                <Form.Control type="date" placeholder="Upload Floor Layout Picture" value={keys === -1 ? '' : dummy[keys].date}/>
              </Col>
            </Row>
            <Row className="mb-2">
              <Form.Control type="text" placeholder="Income" value={keys === -1 ? '' : dummy[keys].income}/>
            </Row>
            <Row className="mb-2">
              <Form.Control type="text" placeholder="Cost" value={keys === -1 ? '' : dummy[keys].costs}/>
            </Row>
            <Row className="mb-2">
              <Form.Control type="text" placeholder="Profit" value={keys === -1 ? '' : dummy[keys].profit}/>
            </Row>
            <Row className="mb-2">
              <Form.Control type="file" placeholder="Upload"/>
            </Row>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal> 
      {/* [ Main Content ] end */}
    </>
  );
};

export default Managementfinancial;

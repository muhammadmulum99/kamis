import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const FormAddHotelProperty = () => {
  const initialState = {
    uuid_properti_hotel: '',
    jenis_hotel: '',
    alamat_hotel: '',
    provinsi_hotel: '',
    kecamatan_hotel: '',
    long_lat_hotel: '',
    id_owner: '',
    foto_hotel: '',
    fasilitas: '',
  }
  const [data, setData] = useState(initialState);
  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  // const [status, setStatus] = useState("");
  const [msg, setMsg] = useState("");
  const navigate = useNavigate();

  const saveUser = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/propertihotel", data);
      navigate("/hotelproperty");
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  };
  const uploadFiles = (e) => {
    var f = e.target.files[0]; // FileList object
    var reader = new FileReader();
    // Closure to capture the file information.
    reader.onload = (function(theFile) {
      return function(e) {
        var binaryData = e.target.result;
        //Converting Binary Data to base 64
        var base64String = window.btoa(binaryData);
        //showing file converted to base64
        // document.getElementById('base64').value = base64String;
        setData({
          ...data,
          foto_hotel: base64String,
        })
        // alert('File converted to base64 successfuly!\nCheck in Textarea');
      };
    })(f);
    // Read in the image file as a data URL.
    reader.readAsBinaryString(f);
  }
  return (
    <div>
      <h1 className="title">Hotel Property</h1>
      <h2 className="subtitle">Add New Hotel Property</h2>
      <div className="card is-shadowless">
        <div className="card-content">
          <div className="content">
            <form onSubmit={saveUser}>
              <p className="has-text-centered">{msg}</p>
              <div className="field">
                <label className="label">ID Hotel Property</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="uuid_properti_hotel"
                    value={data.uuid_properti_hotel}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Property Type</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="jenis_hotel"
                    value={data.jenis_hotel}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Address</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="alamat_hotel"
                    value={data.alamat_hotel}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Province</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="provinsi_hotel"
                    value={data.provinsi_hotel}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Kecamatan</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="kecamatan_hotel"
                    value={data.kecamatan_hotel}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Long Lat</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="long_lat_hotel"
                    value={data.long_lat_hotel}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">ID Owner</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="id_owner"
                    value={data.id_owner}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Foto Hotel</label>
                <div className="control">
                  <input
                    type="file"
                    className="input"
                    name="foto_hotel"
                    onChange={uploadFiles}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Fasilitas</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="fasilitas"
                    value={data.fasilitas}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>

              <div className="field">
                <div className="control">
                  <button type="submit" className="button is-success">
                    Save
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormAddHotelProperty;

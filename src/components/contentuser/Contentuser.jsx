import React from "react";
import "./contentuser.css";
import { useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import { MdDateRange } from "react-icons/md";
import { TfiLocationPin } from "react-icons/tfi";
import { FaCity, FaBed, FaSearchLocation } from "react-icons/fa";
import { DateRange } from "react-date-range";
import { format } from "date-fns";
import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css";
import axios from "axios";
import cryptoJs from "crypto-js";
const Contentuser = () => {
  const lokasi = useFormInput("");

  const lokasiRef = useRef(null);
  const [updated, setUpdated] = useState("");
  // const handleClick = () => {
  //   //  "inputRef.current.value" is input value
  //   setUpdated(inputRef.current.value);
  // };
  const [destination, setDestination] = useState("");
  const [openDate, setOpenDate] = useState(false);
  const [date, setDate] = useState([
    {
      startDate: new Date(),
      endDate: new Date(),
      key: "selection",
    },
  ]);
  const tanggal_checkin = format(date[0].startDate, "yyyy-MM-dd");
  const tanggal_checkout = format(date[0].endDate, "yyyy-MM-dd");
  const [openOptions, setOpenOptions] = useState(false);
  const [options, setOptions] = useState({
    adult: 1,
    children: 0,
    room: 1,
  });
  const navigate = useNavigate();
  const handleOption = (name, operation) => {
    setOptions((prev) => {
      return {
        ...prev,
        [name]: operation === "i" ? options[name] + 1 : options[name] - 1,
      };
    });
  };

  const getInputValue = (event) => {
    const userValue = event.target.value;
    console.log(userValue);
  };

  const handleSearch = () => {
    // const api = "http://localhost:80/backend_cijwt/ApiPost/logUser";
    const url = "http://localhost:5000/searchhotel";
    // const token = localStorage.getItem("session");
    const checkin = tanggal_checkin;
    const checkout = tanggal_checkout;
    // const lokasi = setUpdated(inputRef.current.value);
    // const headers = {
    //   authorization: token,
    //   Accept: "aplication/json",
    //   "Content-type": "application/json",
    //   "Content-Type": "application/x-www-form-urlencoded",
    // };
    const params = {
      tgl_checkin: checkin,
      tgl_checkout: checkout,
      lokasi: lokasiRef.current.value,
    };

    axios
      .get(
        url,
        {
          params,
        },
        // {
        //   headers: {
        //     authorization: token,
        //     Accept: "aplication/json",
        //     "Content-type": "application/json",
        //     "Content-Type": "application/x-www-form-urlencoded",
        //   },
        // }
      )
      .then(function (responses) {
        // setData(JSON.stringify(responses.data.data));
        // alert(JSON.stringify(responses.data.data));
        // console.log(responses.data.data);
        const bytes =  cryptoJs.AES.decrypt(responses.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        localStorage.setItem('hotels', JSON.stringify(decryptedData))
        navigate("/hotels");
      })
      .catch(function (responses) {
        console.log(responses);
      });

    // navigate("/hotels", { state: { destination, date, options } });
  };
  return (
    <>
      <div className="main-content">
        <div className="search-main">
          <div className="headerSearch">
            <div className="headerSearchItem">
              <TfiLocationPin className="headerIcon" />
              <input
                type="text"
                placeholder="Enter City, Hotels or Location?"
                className="headerSearchInput"
                // onChange={getInputValue}
                ref={lokasiRef}
                // type="text"
                id="message"
                name="message"
                autoComplete="off"
                // value={state.value}
                // onChange={(e) => handleChange(e)}
                // autoComplete="new-password"
                // onChange={(e) => setDestination(e.target.value)}
              />
            </div>
            <div className="headerSearchItem">
              <MdDateRange className="headerIcon" />
              <input
                type="hidden"
                placeholder=""
                className="headerSearchInput"
                value={date[0].startDate}
                {...tanggal_checkin}
                autoComplete="new-password"
                // onChange={(e) => setDestination(e.target.value)}
              />
              <input
                type="hidden"
                placeholder=""
                className="headerSearchInput"
                value={date[0].startDate}
                {...tanggal_checkout}
                autoComplete="new-password"
                // onChange={(e) => setDestination(e.target.value)}
              />
              <span
                onClick={() => setOpenDate(!openDate)}
                className="headerSearchText"
              >{`${format(date[0].startDate, "yyyy-MM-dd")} to ${format(
                date[0].endDate,
                "yyyy-MM-dd"
              )}`}</span>
              {openDate && (
                <DateRange
                  editableDateInputs={true}
                  onChange={(item) => setDate([item.selection])}
                  moveRangeOnFirstSelection={false}
                  ranges={date}
                  className="date"
                  minDate={new Date()}
                />
              )}
            </div>
            <div className="headerSearchItem">
              <FaBed className="headerIcon" />
              <span
                onClick={() => setOpenOptions(!openOptions)}
                className="headerSearchText"
              >{`${options.adult} adult · ${options.children} children · ${options.room} room`}</span>
              {openOptions && (
                <div className="options">
                  <div className="optionItem">
                    <span className="optionText">Adult</span>
                    <div className="optionCounter">
                      <button
                        disabled={options.adult <= 1}
                        className="optionCounterButton"
                        onClick={() => handleOption("adult", "d")}
                      >
                        -
                      </button>
                      <span className="optionCounterNumber">
                        {options.adult}
                      </span>
                      <button
                        className="optionCounterButton"
                        onClick={() => handleOption("adult", "i")}
                      >
                        +
                      </button>
                    </div>
                  </div>
                  <div className="optionItem">
                    <span className="optionText">Children</span>
                    <div className="optionCounter">
                      <button
                        disabled={options.children <= 0}
                        className="optionCounterButton"
                        onClick={() => handleOption("children", "d")}
                      >
                        -
                      </button>
                      <span className="optionCounterNumber">
                        {options.children}
                      </span>
                      <button
                        className="optionCounterButton"
                        onClick={() => handleOption("children", "i")}
                      >
                        +
                      </button>
                    </div>
                  </div>
                  <div className="optionItem">
                    <span className="optionText">Room</span>
                    <div className="optionCounter">
                      <button
                        disabled={options.room <= 1}
                        className="optionCounterButton"
                        onClick={() => handleOption("room", "d")}
                      >
                        -
                      </button>
                      <span className="optionCounterNumber">
                        {options.room}
                      </span>
                      <button
                        className="optionCounterButton"
                        onClick={() => handleOption("room", "i")}
                      >
                        +
                      </button>
                    </div>
                  </div>
                </div>
              )}
            </div>
            <div className="headerSearchItem">
              <button className="btn btn-warning" onClick={handleSearch}>
                <FaSearchLocation /> Search
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const useFormInput = (initialValue) => {
  const [value, setValue] = useState(initialValue);

  const handleChange = (e) => {
    setValue(e.target.value);
  };
  return {
    value,
    onChange: handleChange,
  };
};
export default Contentuser;

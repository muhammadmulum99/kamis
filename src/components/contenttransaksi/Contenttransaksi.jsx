import React, { useState } from "react";
import { Container, Row, Col, Form, Button, Card } from "react-bootstrap";
import "./contenttransaksi.css";
import { BsStar, BsDoorOpen } from "react-icons/bs";
import { MdOutlineDateRange } from "react-icons/md";
import { BiBed } from "react-icons/bi";
import logo from "./benderaindo.jpg";
import { useNavigate } from "react-router-dom";
const Contenttransaksi = () => {
  let detail = localStorage.getItem('transaksi') ? JSON.parse(localStorage.getItem('transaksi')) :
  {
    booking_id: "",
    name: "",
    dob: "",
    email: "",
    phone_number: "",
    nik_passport: "",
    address: "",
    special_need: "",
    room_name: "",
    id_properti_hotel: '',
    id_properti_kamar: '',
    room_type: '',
    room_number: "",
    start_date: "",
    end_date: "",
    total_biaya: '',
    room_price: '',
    jumlah_kamar: '',
    room_price: '',
    jenis_hotel: '',
    alamat_hotel: '',
    additions: "",
    platfrom_fee: "",
    ppn: "",
    tgl_checkin: '',
    tgl_checkout: '',
    no_ref: "",
    sumber: "",
    kode_promo: "",
    jenis_pembayaran: "",
    id_user: detail.id_user
  }
  const navigasi = useNavigate();
  const initialState = {
    booking_id: "",
    name: "",
    dob: "",
    email: "",
    phone_number: "",
    nik_passport: "",
    address: "",
    special_need: "",
    room_name: "",
    jenis_kelamin: '',
    id_properti_hotel: detail.id_properti_hotel,
    id_properti_kamar: detail.id_properti_kamar,
    room_type: detail.room_type,
    room_number: "",
    start_date: "",
    end_date: "",
    total_biaya: detail.total_biaya,
    jumlah_kamar: detail.jumlah_kamar,
    room_price: detail.harga_kamar,
    jenis_hotel: detail.jenis_hotel,
    alamat_hotel: detail.alamat_hotel,
    additions: "",
    platfrom_fee: "",
    ppn: "",
    tgl_checkin: detail.tgl_checkin,
    tgl_checkout: detail.tgl_checkout,
    no_ref: "",
    sumber: "",
    kode_promo: "",
    jenis_pembayaran: "",
    id_user: detail.id_user
  }

  const [input, setInput] = useState(initialState);

  const handleInputChange = (event) => {
    setInput({
      ...input,
      [event.target.name]: event.target.value,
    });
  }
  const handletransaksi = () => {
    localStorage.setItem('payment', JSON.stringify(input))
    navigasi("/payment");
  };
  return (
    <>
      <div className="main-content-transaksi">
        <Container>
          {/* Stack the columns on mobile by making one full-width and the other half-width */}
          <Row>
            <Col xs={12} md={8} style={{}}>
              <Card
                style={{
                  width: "600px",
                  boxShadow: "0 10px 40px #0000004d",
                  borderRadius: "30px",
                  position: "sticky",
                }}
              >
                <div className="main-contenttransaksi">
                  <p>Transaction Details</p>
                </div>
                <Card.Body>
                  <Form>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Label>Name</Form.Label>
                      <Form.Control type="text" name="name" onChange={handleInputChange} placeholder="Full Name" />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Date of Birth</Form.Label>
                      <Form.Control type="date" name="dob" onChange={handleInputChange} placeholder="Date of Birth" />
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>Gender</Form.Label>
                      <Form.Select name="jenis_kelamin" onChange={handleInputChange}>
                        <option value="">Please choose</option>
                        <option>Male</option>
                        <option>Female</option>
                      </Form.Select>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Email</Form.Label>
                      <Form.Control type="email" name="email" onChange={handleInputChange} placeholder="Email" />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Phone Number</Form.Label>
                      <Form.Control type="text" name="phone_number" onChange={handleInputChange} placeholder="Phone Number" />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>NIK/Passport Number</Form.Label>
                      <Form.Control
                        type="text"
                        name="nik_passport" onChange={handleInputChange}
                        placeholder="NIK/Passport Number"
                      />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Social Needs</Form.Label>
                      <Form.Control type="text" name="special_needs" onChange={handleInputChange} placeholder="Special Needs" />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicCheckbox">
                      <Form.Check
                        type="checkbox"
                        label="I have read and accept the Terms and Conditions"
                      />
                    </Form.Group>
                    <Button variant="outline-warning" onClick={handletransaksi}>
                      Pay as Guest
                    </Button>{" "}
                    <Button variant="warning">Login</Button>{" "}
                  </Form>
                </Card.Body>
              </Card>
            </Col>

            <Col xs={6} md={4} style={{ position: "sticky" }}>
              <Card
                style={{
                  boxShadow: "0 10px 40px #0000004d",
                  borderRadius: "30px",
                  position: "sticky",
                  width: "450px",
                }}
              >
                <Card.Body>
                  <div className="main-contentkiriform">
                    <div className="contentkiri-fotonama">
                      <img src={logo} alt="" />
                      <div className="hotelnama">
                        <h3>{input.jenis_hotel}</h3>
                        <p>{input.alamat_hotel}</p>
                      </div>
                    </div>
                    <div className="contentkiri-border">
                      <div className="contentkiri-checkin">
                        <div className="checkin">
                          <h3>Check In</h3>
                          <p>{input.tgl_checkin}</p>
                        </div>
                        <div className="checkout">
                          <h3>Check Out</h3>
                          <p>{input.tgl_checkout}</p>
                        </div>
                      </div>
                      <hr align="left" width="394" />
                      <div className="contentkiri-room">
                        <div className="contentkiri-roomtype">
                          <h3>Room type</h3>
                          <p>{input.room_type}</p>
                        </div>
                        <div className="contentkiri-roomroom">
                          <h3>Room</h3>
                          <p>x {input.jumlah_kamar}</p>
                        </div>
                      </div>
                    </div>
                    <div className="contentkiri-roomprice">
                      <h3>Room price</h3>
                      <p>Rp. {input.room_price}</p>
                    </div>
                    <div className="contentkiri-ppn">
                      <h3>PPN</h3>
                      <p>{input.ppn}</p>
                    </div>
                    <div className="contentkiri-fee">
                      <h3>Platform fee</h3>
                      <p>{}</p>
                    </div>
                    <div className="contentkiri-promo">
                      <h3>Promo</h3>
                      <p>{}</p>
                    </div>
                    <div className="contentkiri-refund">
                      <h3>Refund</h3>
                      <p>Not Availble</p>
                    </div>
                    <hr align="left" width="394" />
                    <div className="contentkiri-price">
                      <h3>Total Price</h3>
                      <p>Rp. {input.total_biaya}</p>
                    </div>
                  </div>
                </Card.Body>
              </Card>
            </Col>
          </Row>

          {/* Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop */}

          {/* Columns are always 50% wide, on mobile and desktop */}
        </Container>
      </div>
    </>
  );
};

export default Contenttransaksi;

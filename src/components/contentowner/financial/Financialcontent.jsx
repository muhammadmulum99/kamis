import React, { useState, useEffect } from "react";
import CanvasJSReact from "./canvasjs.react";
import {
  Button,
  Table,
  Dropdown,
  Image,
  Card,
  Modal,
  Form,
  Row,
  Col,
} from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { getMe } from "../../../features/authSlice";
import cryptoJs from "crypto-js";
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

const Financialcontent = () => {
  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);
  const navigate = useNavigate();
  const initialState = {
    // uuid_staff: "",
    id: "",
    nama_staff: "",
    email: "",
    phone_number: "",
    position: "",
    username: "",
    password: "",
    id_owner: "",
    id_management: "",
    id_properti_hotel: "",
    address: "",
  };

  const [data, setData] = useState(initialState);
  const [staff, setStaff] = useState([]);

  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      // navigate("/");
    }
    if (user) {
      setData({ ...data, id_management: user.uuid });
    }
  }, [isError, user, navigate]);

  const getHotel = async () => {
    const response = await axios.get(
      `http://localhost:5000/managementhotel/${data.id_management}`
    );
    const bytes = cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
    if (decryptedData) {
      setData({
        ...data,
        id_properti_hotel: decryptedData[0].uuid_properti_hotel,
      });
    }
  };

  const getStaff = async () => {
    const response = await axios.get(
      `http://localhost:5000/managementstaff/${data.id_management}`
    );
    const bytes = cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
    if (decryptedData) {
      setStaff(decryptedData);
    }
  };

  useEffect(() => {
    if (data.id_management === "") return;
    getHotel();
    getStaff();
  }, [data.id_management]);

  const saveStaff = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/createstaffmanagement", data);
      getStaff();
      setData(initialState);
      // handleClose();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const updateStaff = async (e) => {
    e.preventDefault();
    try {
      await axios.patch(
        "http://localhost:5000/updatestaffmanagement/" + data.id,
        data
      );
      getStaff();
      setData(initialState);
      // handleClose();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const setInputFromTable = (key) => {
    setData({
      ...data,
      id: staff[key].id,
      nama_staff: staff[key].nama_staff,
      email: staff[key].email,
      phone_number: staff[key].phone_number,
      position: staff[key].position,
      username: staff[key].username,
      password: staff[key].password,
      id_owner: staff[key].id_owner,
      id_management: staff[key].id_management,
      id_properti_hotel: staff[key].id_properti_hotel,
    });
  };

  const dummy = [
    {
      time: "10:00",
      date: "2022-10-13",
      income: "2000000",
      costs: "100000",
      profit: "3000000",
    },
  ];

  const options = {
    animationEnabled: true,
    title: {
      text: "Grafik Pendapatan",
    },
    axisY: {
      title: "total",
      suffix: "mn",
    },
    toolTip: {
      shared: "true",
    },
    legend: {
      cursor: "pointer",
      // itemclick : toggleDataSeries
    },
    data: [
      {
        type: "spline",
        visible: false,
        showInLegend: true,
        yValueFormatString: "##.00mn",
        name: "Season 1",
        dataPoints: [
          { label: "Ep. 1", y: 2.22 },
          { label: "Ep. 2", y: 2.2 },
          { label: "Ep. 3", y: 2.44 },
          { label: "Ep. 4", y: 2.45 },
          { label: "Ep. 5", y: 2.58 },
          { label: "Ep. 6", y: 2.44 },
          { label: "Ep. 7", y: 2.4 },
          { label: "Ep. 8", y: 2.72 },
          { label: "Ep. 9", y: 2.66 },
          { label: "Ep. 10", y: 3.04 },
        ],
      },
      {
        type: "spline",
        showInLegend: true,
        visible: false,
        yValueFormatString: "##.00mn",
        name: "Season 2",
        dataPoints: [
          { label: "Ep. 1", y: 3.86 },
          { label: "Ep. 2", y: 3.76 },
          { label: "Ep. 3", y: 3.77 },
          { label: "Ep. 4", y: 3.65 },
          { label: "Ep. 5", y: 3.9 },
          { label: "Ep. 6", y: 3.88 },
          { label: "Ep. 7", y: 3.69 },
          { label: "Ep. 8", y: 3.86 },
          { label: "Ep. 9", y: 3.38 },
          { label: "Ep. 10", y: 4.2 },
        ],
      },
      {
        type: "spline",
        visible: false,
        showInLegend: true,
        yValueFormatString: "##.00mn",
        name: "Season 3",
        dataPoints: [
          { label: "Ep. 1", y: 4.37 },
          { label: "Ep. 2", y: 4.27 },
          { label: "Ep. 3", y: 4.72 },
          { label: "Ep. 4", y: 4.87 },
          { label: "Ep. 5", y: 5.35 },
          { label: "Ep. 6", y: 5.5 },
          { label: "Ep. 7", y: 4.84 },
          { label: "Ep. 8", y: 4.13 },
          { label: "Ep. 9", y: 5.22 },
          { label: "Ep. 10", y: 5.39 },
        ],
      },
      {
        type: "spline",
        visible: false,
        showInLegend: true,
        yValueFormatString: "##.00mn",
        name: "Season 4",
        dataPoints: [
          { label: "Ep. 1", y: 6.64 },
          { label: "Ep. 2", y: 6.31 },
          { label: "Ep. 3", y: 6.59 },
          { label: "Ep. 4", y: 6.95 },
          { label: "Ep. 5", y: 7.16 },
          { label: "Ep. 6", y: 6.4 },
          { label: "Ep. 7", y: 7.2 },
          { label: "Ep. 8", y: 7.17 },
          { label: "Ep. 9", y: 6.95 },
          { label: "Ep. 10", y: 7.09 },
        ],
      },
      {
        type: "spline",
        showInLegend: true,
        yValueFormatString: "##.00mn",
        name: "Season 5",
        dataPoints: [
          { label: "Ep. 1", y: 8 },
          { label: "Ep. 2", y: 6.81 },
          { label: "Ep. 3", y: 6.71 },
          { label: "Ep. 4", y: 6.82 },
          { label: "Ep. 5", y: 6.56 },
          { label: "Ep. 6", y: 6.24 },
          { label: "Ep. 7", y: 5.4 },
          { label: "Ep. 8", y: 7.01 },
          { label: "Ep. 9", y: 7.14 },
          { label: "Ep. 10", y: 8.11 },
        ],
      },
      {
        type: "spline",
        showInLegend: true,
        yValueFormatString: "##.00mn",
        name: "Season 6",
        dataPoints: [
          { label: "Ep. 1", y: 7.94 },
          { label: "Ep. 2", y: 7.29 },
          { label: "Ep. 3", y: 7.28 },
          { label: "Ep. 4", y: 7.82 },
          { label: "Ep. 5", y: 7.89 },
          { label: "Ep. 6", y: 6.71 },
          { label: "Ep. 7", y: 7.8 },
          { label: "Ep. 8", y: 7.6 },
          { label: "Ep. 9", y: 7.66 },
          { label: "Ep. 10", y: 8.89 },
        ],
      },
      {
        type: "spline",
        showInLegend: true,
        yValueFormatString: "##.00mn",
        name: "Season 7",
        dataPoints: [
          { label: "Ep. 1", y: 10.11 },
          { label: "Ep. 2", y: 9.27 },
          { label: "Ep. 3", y: 9.25 },
          { label: "Ep. 4", y: 10.17 },
          { label: "Ep. 5", y: 10.72 },
          { label: "Ep. 6", y: 10.24 },
          { label: "Ep. 7", y: 12.07 },
        ],
      },
      {
        type: "spline",
        showInLegend: true,
        yValueFormatString: "##.00mn",
        name: "Season 8",
        dataPoints: [
          { label: "Ep. 1", y: 11.76 },
          { label: "Ep. 2", y: 10.29 },
          { label: "Ep. 3", y: 12.02 },
          { label: "Ep. 4", y: 11.8 },
          { label: "Ep. 5", y: 12.48 },
          { label: "Ep. 6", y: 13.61 },
        ],
      },
    ],
  };
  return (
    <>
      {/* [ Main Content ] start */}
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              {/* [ breadcrumb ] start */}
              <div class="page-header">
                <div class="page-block">
                  <div class="row align-items-center">
                    <div class="col-md-12">
                      <div class="page-header-title">
                        <h5 class="m-b-10">Typography</h5>
                      </div>
                      <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="index.html">
                            <i class="feather icon-home"></i>
                          </a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Basic Componants</a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Typography</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/* [ breadcrumb ] end */}
              <div class="main-body">
                <div class="page-wrapper">
                  {/* [ Main Content ] start */}
                  <div class="row">
                    {/* [ Typography ] start */}
                    <div class="col-sm-12">
                      <div class="card">
                        <div class="card-header">test</div>
                        <div class="card-body">
                          <CanvasJSChart
                            options={options}
                            /* onRef={ref => this.chart = ref} */
                          />
                        </div>
                        <div class="card-body">
                          <Row>
                            <Col md={8}>
                              <Table striped bordered hover>
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Time</th>
                                    <th>Date</th>
                                    <th>Income</th>
                                    <th>Conts</th>
                                    <th>Profit</th>
                                    {/* <th>Details</th> */}
                                  </tr>
                                </thead>
                                <tbody>
                                  {dummy.map((item, key) => {
                                    return (
                                      <tr>
                                        <td>{key}</td>
                                        <td>{item.time}</td>
                                        <td>{item.date}</td>
                                        <td>{item.income}</td>
                                        <td>{item.costs}</td>
                                        <td>{item.profit}</td>
                                        {/* <td>
                                          <Button 
                                          onClick={() => {
                                            setModal(true)
                                            setKeys(key)
                                          }}>
                                            Edit
                                          </Button>
                                          <Button>
                                            Download
                                          </Button>
                                        </td> */}
                                      </tr>
                                    );
                                  })}
                                </tbody>
                              </Table>
                            </Col>
                            <Col md={4}>
                              <Row>Total Overview</Row>
                              <Row>Total Income</Row>
                              <Row>Total Costs</Row>
                              <Row>Total Profit</Row>
                              <Row>Total Overall</Row>
                            </Col>
                          </Row>
                        </div>
                      </div>
                    </div>

                    {/* [ Typography ] end */}
                  </div>
                  {/* [ Main Content ] end */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* [ Main Content ] end */}
    </>
  );
};

export default Financialcontent;

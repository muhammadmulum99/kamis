import React, { useState, useEffect } from "react";
import { Row, Table } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { getMe } from "../../../features/authSlice";
import cryptoJs from "crypto-js";

const Ownerprofile = () => {
  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);
  const navigate = useNavigate();
  const initialState = {
    // uuid_staff: "",
    id: "",
    nama_staff: "",
    email: "",
    phone_number: "",
    position: "",
    username: "",
    password: "",
    id_owner: "",
    id_management: "",
    id_properti_hotel: "",
    address: "",
  }

  const [data, setData] = useState(initialState)
  const [staff, setStaff] = useState([])

  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  
  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      // navigate("/");
    }
    if(user){
      setData({...data, id_management: user.uuid})
    }
  }, [isError, user, navigate]);

   const getHotel = async () => {
    const response = await axios.get(`http://localhost:5000/managementhotel/${data.id_management}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setData({
        ...data,
        id_properti_hotel: decryptedData[0].uuid_properti_hotel
      })
    }    
  }

  const getStaff = async () => {
    const response = await axios.get(`http://localhost:5000/managementstaff/${data.id_management}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setStaff(decryptedData)
    }    
  }

  useEffect(() => {
    if(data.id_management === '') return;
    getHotel();
    getStaff();
  },[data.id_management])

  const saveStaff = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/createstaffmanagement", data);
      getStaff();
      setData(initialState)
      // handleClose();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const updateStaff = async (e) => {
    e.preventDefault();
    try {
      await axios.patch("http://localhost:5000/updatestaffmanagement/"+data.id, data);
      getStaff();
      setData(initialState);
      // handleClose();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const setInputFromTable = (key) => {
    setData({
      ...data,
      id: staff[key].id,
      nama_staff: staff[key].nama_staff,
      email: staff[key].email,
      phone_number: staff[key].phone_number,
      position: staff[key].position,
      username: staff[key].username,
      password: staff[key].password,
      id_owner: staff[key].id_owner,
      id_management: staff[key].id_management,
      id_properti_hotel: staff[key].id_properti_hotel,
    })
  }

  return (
    <>
      {/* [ Main Content ] start */}
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              {/* [ breadcrumb ] start */}
              <div class="page-header">
                <div class="page-block">
                  <div class="row align-items-center">
                    <div class="col-md-12">
                      <div class="page-header-title">
                        <h5 class="m-b-10">Typography</h5>
                      </div>
                      <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="index.html">
                            <i class="feather icon-home"></i>
                          </a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Basic Componants</a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Typography</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/* [ breadcrumb ] end */}
              <div class="main-body">
                <div class="page-wrapper">
                  {/* [ Main Content ] start */}
                  <div class="row">
                    {/* [ Typography ] start */}
                    <div class="col-sm-12">
                      <div class="card">
                        <div class="card-header">
                          <Row>
                            <h5>Profile</h5>
                          </Row>                          
                        </div>
                        <div class="card-body">
                          <Row>
                            <h6>Owner Name</h6>
                          </Row>
                          <Row>
                            <p>Owner Details</p>
                          </Row>
                          <Row>
                            <p>Total Property Owned</p>
                          </Row>
                          <Row>
                            <p>Total Staff</p>
                          </Row>
                          <select className="mb-2 ">
                            <option>Property 1</option>
                            <option>Property 2</option>
                            <option>Property 3</option>
                          </select>
                          <p>Property Name</p>
                          <p>Property Address</p>
                          <h6>Facilities</h6>
                          <div class="col-sm-6">
                            <Table striped bordered hover>
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Name Facility</th>
                                  <th>Status Facilities</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>1</td>
                                  <td>Gym</td>
                                  <td>Open</td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td>Swiming Pool</td>
                                  <td>Maintenence</td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td>Restaurant</td>
                                  <td>Closed</td>
                                </tr>
                              </tbody>
                            </Table>
                          </div>

                          <h6>Rooms</h6>
                          <div class="row">
                            <div class="col-sm-4">
                              <Table striped bordered hover>
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Status Room</th>
                                    <th>Jumlah</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>1</td>
                                    <td>Room Open</td>
                                    <td>20</td>
                                  </tr>
                                  <tr>
                                    <td>2</td>
                                    <td>Room Booked</td>
                                    <td>10</td>
                                  </tr>
                                  <tr>
                                    <td>3</td>
                                    <td>Rooms in use</td>
                                    <td>5</td>
                                  </tr>
                                  <tr>
                                    <td>4</td>
                                    <td>Rooms Under Maintenence</td>
                                    <td>0</td>
                                  </tr>
                                </tbody>
                              </Table>
                            </div>
                            <div class="col-sm-4">
                              <Table striped bordered hover>
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Status Room</th>
                                    <th>Jumlah</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>1</td>
                                    <td>Room Open</td>
                                    <td>20</td>
                                  </tr>
                                  <tr>
                                    <td>2</td>
                                    <td>Room Booked</td>
                                    <td>10</td>
                                  </tr>
                                  <tr>
                                    <td>3</td>
                                    <td>Rooms in use</td>
                                    <td>5</td>
                                  </tr>
                                  <tr>
                                    <td>4</td>
                                    <td>Rooms Under Maintenence</td>
                                    <td>0</td>
                                  </tr>
                                </tbody>
                              </Table>
                            </div>
                            <div class="col-sm-4">
                              <Table striped bordered hover>
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Status Room</th>
                                    <th>Jumlah</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>1</td>
                                    <td>Room Open</td>
                                    <td>20</td>
                                  </tr>
                                  <tr>
                                    <td>2</td>
                                    <td>Room Booked</td>
                                    <td>10</td>
                                  </tr>
                                  <tr>
                                    <td>3</td>
                                    <td>Rooms in use</td>
                                    <td>5</td>
                                  </tr>
                                  <tr>
                                    <td>4</td>
                                    <td>Rooms Under Maintenence</td>
                                    <td>0</td>
                                  </tr>
                                </tbody>
                              </Table>
                            </div>
                          </div>

                          {/* <h6>Staff</h6>
                          <div class="col-sm-6">
                            <Table striped bordered hover>
                              <thead>
                                <tr>
                                  <th>Status Staff</th>
                                  <th>Jumlah</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Total Staff</td>
                                  <td>100</td>
                                </tr>
                                <tr>
                                  <td>On Shift</td>
                                  <td>50</td>
                                </tr>
                                <tr>
                                  <td>Off Shift</td>
                                  <td>25</td>
                                </tr>
                                <tr>
                                  <td>Sick Leave</td>
                                  <td>5</td>
                                </tr>
                                <tr>
                                  <td>Vacation</td>
                                  <td>15</td>
                                </tr>
                                <tr>
                                  <td>Other</td>
                                  <td>50</td>
                                </tr>
                              </tbody>
                            </Table>
                          </div>

                          <h6>Last Financial Update : 2022/10/14</h6>
                          <h6>Last Report Update : 2022/10/14</h6> */}
                        </div>
                      </div>
                    </div>

                    {/* [ Typography ] end */}
                  </div>
                  {/* [ Main Content ] end */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* [ Main Content ] end */}
    </>
  );
};

export default Ownerprofile;

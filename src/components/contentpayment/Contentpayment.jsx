import React, { useEffect, useState } from "react";
import { Container, Row, Col, Form, Button, Card } from "react-bootstrap";
import "./contentpayment.css";
import { BsStar, BsDoorOpen } from "react-icons/bs";
import { MdOutlineDateRange } from "react-icons/md";
import { BiBed } from "react-icons/bi";
import logo from "./benderaindo.jpg";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { getMe } from "../../features/authSlice";
import { useDispatch, useSelector } from "react-redux";

const Contentpayment = () => {
  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);
  let detail = localStorage.getItem('payment') ? JSON.parse(localStorage.getItem('payment')) :
  {
    booking_id: "",
    jenis_kelamin: "",
    name: "",
    dob: "",
    email: "",
    phone_number: "",
    nik_passport: "",
    address: "",
    special_need: "",
    room_name: "",
    id_properti_hotel: '',
    id_properti_kamar: '',
    room_type: '',
    room_number: "",
    start_date: "",
    end_date: "",
    total_biaya: '',
    room_price: '',
    jumlah_kamar: '',
    jenis_hotel: '',
    alamat_hotel: '',
    additions: "",
    platfrom_fee: "",
    ppn: "",
    tgl_checkin: '',
    tgl_checkout: '',
    no_ref: "",
    sumber: "",
    kode_promo: "",
    jenis_pembayaran: "",
    id_user: detail.id_user
  }
  const initialState = {
    booking_id: "",
    jenis_kelamin: detail.jenis_kelamin,
    tgl_transaksi: new Date().getDate() + '/' + new Date().getMonth() + '/' + new Date().getFullYear(),
    name: detail.name,
    dob: detail.dob,
    email: detail.email,
    phone_number: detail.phone_number,
    nik_passport: detail.nik_passport,
    address: "",
    special_need: detail.special_need,
    room_name: "",
    id_properti_hotel: detail.id_properti_hotel,
    id_properti_kamar: detail.id_properti_kamar,
    room_type: detail.room_type,
    room_number: "",
    start_date: "",
    end_date: "",
    total_biaya: detail.total_biaya,
    room_price: detail.room_price,
    jumlah_kamar: detail.jumlah_kamar,
    jenis_hotel: detail.jenis_hotel,
    alamat_hotel: detail.alamat_hotel,
    additions: "",
    platfrom_fee: "",
    ppn: "",
    tgl_checkin: detail.tgl_checkin,
    tgl_checkout: detail.tgl_checkout,
    no_ref: "",
    sumber: "",
    kode_promo: "",
    jenis_pembayaran: "",
    id_user: detail.id_user,
    bukti_pembayaran: ''
  }
  const navigate = useNavigate();
  const [input, setInput] = useState(initialState);

  const handleInputChange = (event) => {
    setInput({
      ...input,
      [event.target.name]: event.target.value,
    });
  }

  const saveUser = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/transaksi", input);
      navigate("/");
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      navigate("/");
    }
    // if(user){
    //   setData({...data, id_user: user.uuid})
    // }
  }, [isError, user, navigate]);

  const uploadFiles = (e) => {
    var f = e.target.files[0]; // FileList object
    var reader = new FileReader();
    // Closure to capture the file information.
    reader.onload = (function(theFile) {
      return function(e) {
        var binaryData = e.target.result;
        //Converting Binary Data to base 64
        var base64String = window.btoa(binaryData);
        //showing file converted to base64
        // document.getElementById('base64').value = base64String;
        setInput({
          ...input,
          bukti_pembayaran: base64String,
        })
        // alert('File converted to base64 successfuly!\nCheck in Textarea');
      };
    })(f);
    // Read in the image file as a data URL.
    reader.readAsBinaryString(f);
  }

  return (
    <>
      <div className="main-content-payment">
        <Container>
          {/* Stack the columns on mobile by making one full-width and the other half-width */}
          <Row>
            <Col xs={12} md={8} style={{}}>
              <Card
                style={{
                  width: "600px",
                  boxShadow: "0 10px 40px #0000004d",
                  borderRadius: "30px",
                  position: "sticky",
                }}
              >
                <div className="main-contentpayment">
                  <p>Payment</p>
                </div>
                <Card.Body>
                  <Form>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Label>Name: {input.name}</Form.Label>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Date of Birth: {input.dob}</Form.Label>                      
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>Gender: {input.jenis_kelamin}</Form.Label>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Email: {input.email}</Form.Label>                      
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Phone Number: {input.phone_number}</Form.Label>                      
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>NIK/Passport Number: {input.nik_passport}</Form.Label>                      
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Special Needs: {input.special_need}</Form.Label>                      
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Promo</Form.Label>
                      <Form.Control type="text" placeholder="Promo Code" name="kode_promo" onChange={handleInputChange} />
                    </Form.Group>
                    {/* <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Payment Method</Form.Label>
                      <Form.Check type="checkbox" label="Mandiri" />
                      <Form.Check type="checkbox" label="Mandiri" />
                      <Form.Check type="checkbox" label="Mandiri" />
                      <Form.Check type="checkbox" label="Mandiri" />
                    </Form.Group> */}                    
                    <Form.Group className="mb-3">
                      <Form.Label>Payment Method</Form.Label>
                      <Form.Select name="jenis_pembayaran" onChange={handleInputChange}>
                        <option value="">Pilih Pembayaran</option>
                        <option>COD</option>
                        <option>Bank BRI</option>
                        <option>Bank Mandiri</option>
                        <option>Bank BCA</option>
                        <option>Bank BNI</option>
                        <option>Bank Permata</option>
                      </Form.Select>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Proof of Payment</Form.Label>
                      <Form.Control type="file" placeholder="Promo Code" name="bukti_pembayaran" onChange={uploadFiles} />
                    </Form.Group>
                    <div
                      style={{
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <Button variant="outline-warning" onClick={saveUser}>Pay Now</Button>{" "}
                    </div>
                  </Form>
                </Card.Body>
              </Card>
            </Col>

            <Col xs={6} md={4} style={{ position: "sticky" }}>
              <Card
                style={{
                  boxShadow: "0 10px 40px #0000004d",
                  borderRadius: "30px",
                  position: "sticky",
                  width: "450px",
                }}
              >
                <Card.Body>
                  <div className="main-contentkiriform">
                    <div className="contentkiri-fotonama">
                      <img src={logo} alt="" />
                      <div className="hotelnama">
                        <h3>{input.jenis_hotel}</h3>
                        <p>{input.alamat_hotel}</p>
                      </div>
                    </div>
                    <div className="contentkiri-border">
                      <div className="contentkiri-checkin">
                        <div className="checkin">
                          <h3>Check In</h3>
                          <p>{input.tgl_checkin}</p>
                        </div>
                        <div className="checkout">
                          <h3>Check Out</h3>
                          <p>{input.tgl_checkout}</p>
                        </div>
                      </div>
                      <hr align="left" width="394" />
                      <div className="contentkiri-room">
                        <div className="contentkiri-roomtype">
                          <h3>Room type</h3>
                          <p>{input.room_type}</p>
                        </div>
                        <div className="contentkiri-roomroom">
                          <h3>Room</h3>
                          <p>x {input.jumlah_kamar}</p>
                        </div>
                      </div>
                    </div>
                    <div className="contentkiri-roomprice">
                      <h3>Room price</h3>
                      <p>Rp. {input.room_price}</p>
                    </div>
                    <div className="contentkiri-ppn">
                      <h3>PPN</h3>
                      <p>{input.ppn}</p>
                    </div>
                    <div className="contentkiri-fee">
                      <h3>Platform fee</h3>
                      <p>{}</p>
                    </div>
                    <div className="contentkiri-promo">
                      <h3>Promo</h3>
                      <p>{}</p>
                    </div>
                    <div className="contentkiri-refund">
                      <h3>Refund</h3>
                      <p>Not Availble</p>
                    </div>
                    <hr align="left" width="394" />
                    <div className="contentkiri-price">
                      <h3>Total Price</h3>
                      <p>Rp. {input.total_biaya}</p>
                    </div>
                  </div>
                </Card.Body>
              </Card>
            </Col>
          </Row>

          {/* Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop */}

          {/* Columns are always 50% wide, on mobile and desktop */}
        </Container>
      </div>
    </>
  );
};

export default Contentpayment;

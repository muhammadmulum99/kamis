import React from "react";
import { Navbar, Container, Nav, NavDropdown, Button } from "react-bootstrap";
import logo from "./logo.png";
import { HiOutlineBuildingOffice2 } from "react-icons/hi2";
import { MdOutlineVerified } from "react-icons/md";
import { BsBasket3 } from "react-icons/bs";

const Headerhotels = () => {
  return (
    <>
      <div className="main-header">
        <Navbar bg="light" expand="lg" fixed="top">
          <Container>
            <Navbar.Brand href="#">
              <img
                src={logo}
                width="170px"
                height="56px"
                className="d-inline-block align-top"
              />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto"></Nav>
              <NavDropdown
                title="Language"
                id="basic-nav-dropdown"
                style={{ marginRight: "10px" }}
              >
                <NavDropdown.Item href="#action/3.1">
                  Indonesia
                </NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">English</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">Jepang</NavDropdown.Item>
              </NavDropdown>
              <Button
                href="/login"
                variant="warning"
                style={{
                  right: "0",
                  marginTop: "4px",
                }}
              >
                Login | Signup
              </Button>{" "}
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </div>
    </>
  );
};

export default Headerhotels;

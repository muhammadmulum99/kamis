import React, { useState } from "react";
import logo from "./logo.png";
import { Link } from "react-router-dom";

const Sidebarowner = () => {
  const test = () => {
    console.log("dsd");
  };

  return (
    <>
      <nav class="pcoded-navbar">
        <div class="navbar-wrapper">
          <div class="navbar-brand header-logo">
            <a href="index.html" class="b-brand">
              <img
                src={logo}
                alt=""
                style={{ height: "60px", width: "100%" }}
              />
            </a>
            <a class="mobile-menu" id="mobile-collapse" href="javascript:">
              <span></span>
            </a>
          </div>
          <div class="navbar-content scroll-div">
            <ul class="nav pcoded-inner-navbar">
              {/* <li class="nav-item pcoded-menu-caption">
                <label>Navigation</label>
              </li> */}
              {/* <li
                data-username="dashboard Default Ecommerce CRM Analytics Crypto Project"
                class="nav-item active"
              >
                <a href="index.html" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-home"></i>
                  </span>
                  <span class="pcoded-mtext">Dashboard</span>
                </a>
              </li> */}
              {/* <li class="nav-item pcoded-menu-caption">
                <label>UI Element</label>
              </li> */}
              <li
                onClick={test}
                data-username="basic components Button Alert Badges breadcrumb Paggination progress Tooltip popovers Carousel Cards Collapse Tabs pills Modal Grid System Typography Extra Shadows Embeds"
                class="nav-item pcoded-hasmenu"
              >
                <a href="javascript:" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-box"></i>
                  </span>
                  <span class="pcoded-mtext">Components</span>
                </a>
                <ul class="pcoded-submenu">
                  <li class="">
                    <a href="bc_button.html" class="">
                      Button
                    </a>
                  </li>
                  <li class="">
                    <a href="bc_badges.html" class="">
                      Badges
                    </a>
                  </li>
                  <li class="">
                    <a href="bc_breadcrumb-pagination.html" class="">
                      Breadcrumb & paggination
                    </a>
                  </li>
                  <li class="">
                    <a href="bc_collapse.html" class="">
                      Collapse
                    </a>
                  </li>
                  <li class="">
                    <a href="bc_tabs.html" class="">
                      Tabs & pills
                    </a>
                  </li>
                  <li class="">
                    <a href="bc_typography.html" class="">
                      Typography
                    </a>
                  </li>

                  <li class="">
                    <a href="icon-feather.html" class="">
                      Feather
                      <span class="pcoded-badge label label-danger">NEW</span>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="nav-item pcoded-menu-caption">
                <label>Owner Page</label>
              </li>
              <li
                data-username="form elements advance componant validation masking wizard picker select"
                class="nav-item"
              >
                <a href="/owner-profile" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-user"></i>
                  </span>
                  <span class="pcoded-mtext">Profile</span>
                  {/* <Link to="/owner-profile" style={{ color: "white" }}>
                    Products
                  </Link> */}
                </a>
              </li>
              <li
                data-username="Table bootstrap datatable footable"
                class="nav-item"
              >
                <a href="/owner-properti" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-server"></i>
                  </span>
                  <span class="pcoded-mtext">Property</span>
                </a>
              </li>
              <li
                data-username="Table bootstrap datatable footable"
                class="nav-item"
              >
                <a href="/owner-financial" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-trending-up"></i>
                  </span>
                  <span class="pcoded-mtext">Financial</span>
                </a>
              </li>
              <li
                data-username="Table bootstrap datatable footable"
                class="nav-item"
              >
                <a href="/owner-report" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-file-text"></i>
                  </span>
                  <span class="pcoded-mtext">Report</span>
                </a>
              </li>

              <li
                data-username="Table bootstrap datatable footable"
                class="nav-item"
              >
                <a href="/owner-setting" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-settings"></i>
                  </span>
                  <span class="pcoded-mtext">Setting</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Sidebarowner;

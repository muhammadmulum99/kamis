import React from "react";
import { Navbar, Container, Nav, NavDropdown, Button } from "react-bootstrap";
import logo from "./logo.png";
import { HiOutlineBuildingOffice2 } from "react-icons/hi2";
import { MdOutlineVerified } from "react-icons/md";
import { BsBasket3 } from "react-icons/bs";
// import { getUser, removeUserSession } from "../../../utils/Common";
// const user = getUser();
const Headeruser = () => {
  return (
    <>
      <Navbar bg="light" expand="lg" fixed="top">
        <Container>
          {/* {user.email} */}
          <Navbar.Brand href="#" className="test">
            <img
              src={logo}
              width="170px"
              height="56px"
              className="d-inline-block align-top"
              style={{ background: "light" }}
            />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto"></Nav>
            <Nav.Link href="/owner-properti" style={{ marginRight: "20px" }}>
              <HiOutlineBuildingOffice2
                style={{ marginRight: "5px", marginBottom: "5px" }}
              />{" "}
              List Your Properti
            </Nav.Link>
            <Nav.Link href="/wishlist" style={{ marginRight: "20px" }}>
              <MdOutlineVerified
                style={{ marginRight: "0px", marginBottom: "5px" }}
              />{" "}
              Wishlist
            </Nav.Link>
            <Nav.Link href="/orders" style={{ marginRight: "20px" }}>
              <BsBasket3 style={{ marginRight: "5px", marginBottom: "5px" }} />
              My Orders
            </Nav.Link>
            {/* <NavDropdown
              title="Language"
              id="basic-nav-dropdown"
              style={{ marginRight: "10px" }}
            >
              <NavDropdown.Item href="#action/3.1">Indonesia</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">English</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Jepang</NavDropdown.Item>
            </NavDropdown> */}
            <Button
              href="/login"
              variant="warning"
              style={{
                right: "0",
                marginTop: "4px",
              }}
            >
              Login | Signup
            </Button>{" "}
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};

export default Headeruser;

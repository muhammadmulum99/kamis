import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const FormAddRoomPropertyDetail = () => {
  const initialState = {
    id_properti_hotel: '',
    id_properti_kamar: '',
    uuid_properti_kamar_detail: '',
    nomor_kamar: '',
    lantai_kamar: '',
    status_kamar_detail: '',
  }
  const [data, setData] = useState(initialState);
  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  const [msg, setMsg] = useState("");
  const navigate = useNavigate();

  const saveUser = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/propertikamardetail", data);
      navigate("/roompropertydetail");
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  };
  return (
    <div>
      <h1 className="title">Room</h1>
      <h2 className="subtitle">Add New Room</h2>
      <div className="card is-shadowless">
        <div className="card-content">
          <div className="content">
            <form onSubmit={saveUser}>
              <p className="has-text-centered">{msg}</p>
              <div className="field">
                <label className="label">ID Properti Hotel</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="id_properti_hotel"
                    value={data.id_properti_hotel}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">ID Properti Kamar</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="id_properti_kamar"
                    value={data.id_properti_kamar}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">ID Properti Kamar Detail</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="uuid_properti_kamar_detail"
                    value={data.uuid_properti_kamar_detail}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Nomor Kamar</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="nomor_kamar"
                    value={data.nomor_kamar}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Lantai Kamar</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="lantai_kamar"
                    value={data.lantai_kamar}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Status</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="status_kamar_detail"
                    value={data.status_kamar_detail}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>             
              <div className="field">
                <div className="control">
                  <button type="submit" className="button is-success">
                    Save
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormAddRoomPropertyDetail;

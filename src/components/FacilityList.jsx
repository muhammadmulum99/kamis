import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import cryptoJs from "crypto-js";

const Facilitylist = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  const getUsers = async () => {
    const response = await axios.get("http://localhost:5000/fasilitas");
    console.log(response);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
    setUsers(decryptedData);
  };

  const deleteUser = async (userId) => {
    await axios.delete(`http://localhost:5000/fasilitas/${userId}`);
    getUsers();
  };

  return (
    <div>
      <h1 className="title">Facility</h1>
      <h2 className="subtitle">List of Facility</h2>
      <Link to="/facility/add" className="button is-primary mb-2">
        Add New
      </Link>
      <table className="table is-striped is-fullwidth">
        <thead>
          <tr>
            <th>No</th>
            <th>ID Master Facility</th>
            <th>Name of Master Facility</th>
            <th>Icon of Master Facility</th>
            <th>Type of Master Facility</th>

            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user, index) => (
            <tr key={user.uuid}>
              <td>{index + 1}</td>
              <td>{user.uuid_master_fasilitas}</td>
              <td>{user.nama_master_fasilitas}</td>
              <td>{user.icon_master_fasilitas}</td>
              <td>{user.jenis_master_fasilitas}</td>
              <td>
                <Link
                  to={`/facility/edit/${user.uuid_master_fasilitas}`}
                  className="button is-small is-info"
                >
                  Edit
                </Link>
                <button
                  onClick={() => deleteUser(user.uuid_master_fasilitas)}
                  className="button is-small is-danger"
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Facilitylist;

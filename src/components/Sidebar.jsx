import React from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { IoPerson, IoPricetag, IoHome, IoLogOut } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import { LogOut, reset } from "../features/authSlice";

const Sidebar = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { user } = useSelector((state) => state.auth);

  const logout = () => {
    dispatch(LogOut());
    dispatch(reset());
    navigate("/");
  };

  return (
    <div>
      <aside className="menu pl-2 has-shadow">
        <p className="menu-label">General</p>
        <ul className="menu-list">
          <li>
            <NavLink to={"/dashboard"}>
              <IoHome /> Dashboard
            </NavLink>
          </li>
          <li>
            <NavLink to={"/products"}>
              <IoPricetag /> Products
            </NavLink>
          </li>
        </ul>
        {user && user.role === "admin" && (
          <div>
            <p className="menu-label">Admin</p>
            <ul className="menu-list">
              <li>
                <NavLink to={"/users"}>
                  <IoPerson /> Users
                </NavLink>
              </li>
              <li>
                <NavLink to={"/bank"}>
                  <IoPerson /> Bank
                </NavLink>
              </li>
              <li>
                <NavLink to={"/hotel"}>
                  <IoPerson /> Hotel
                </NavLink>
              </li>
              <li>
                <NavLink to={"/hotelcategory"}>
                  <IoPerson /> Hotel Category
                </NavLink>
              </li>
              <li>
                <NavLink to={"/hotelproperty"}>
                  <IoPerson /> Hotel Property
                </NavLink>
              </li>
              <li>
                <NavLink to={"/room"}>
                  <IoPerson /> Room
                </NavLink>
              </li>
              <li>
                <NavLink to={"/roomproperty"}>
                  <IoPerson /> Room Property
                </NavLink>
              </li>
              <li>
                <NavLink to={"/roompropertydetail"}>
                  <IoPerson /> Room Property Detail
                </NavLink>
              </li>
              <li>
                <NavLink to={"/facility"}>
                  <IoPerson /> Facility
                </NavLink>
              </li>
              <li>
                <NavLink to={"/facilitypropertyextra"}>
                  <IoPerson /> Facility Property Extra
                </NavLink>
              </li>
              <li>
                <NavLink to={"/facilitypropertyhotel"}>
                  <IoPerson /> Facility Property Hotel
                </NavLink>
              </li>
              <li>
                <NavLink to={"/facilitypropertyroom"}>
                  <IoPerson /> Facility Property Room
                </NavLink>
              </li>
            </ul>
          </div>
        )}

        <p className="menu-label">Settings</p>
        <ul className="menu-list">
          <li>
            <button onClick={logout} className="button is-white">
              <IoLogOut /> Logout
            </button>
          </li>
        </ul>
      </aside>
    </div>
  );
};

export default Sidebar;

import React from "react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction";
const Calanderhotel = () => {
  return (
    <FullCalendar
      plugins={[dayGridPlugin, interactionPlugin]}
      initialView="dayGridMonth"
      eventClick={function (arg) {
        alert(arg.event.title);
      }}
      dateClick={function (info) {
        alert('Clicked on: ' + info.dateStr);
      }}
      // events={[
      //   { title: "Penuh", date: "2022-11-01", backgroundColor: "red" },
      //   { title: "Penuh", date: "2022-11-04", backgroundColor: "red" },
      //   { title: "Penuh", date: "2022-11-07", backgroundColor: "red" },
      //   { title: "Penuh", date: "2022-11-15", backgroundColor: "red" },
      //   { title: "Penuh", date: "2022-11-16", backgroundColor: "red" },
      //   { title: "Penuh", date: "2022-11-17", backgroundColor: "red" },
      //   { title: "Penuh", date: "2022-11-20", backgroundColor: "red" },
      // ]}
    />
  );
};

export default Calanderhotel;

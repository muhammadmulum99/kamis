import React from "react";
import { Form, Col, Row, Button, Card } from "react-bootstrap";

const FormPayment = () => {
  return (
    <Form>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Name</Form.Label>
        <Form.Control type="text" placeholder="Full Name" />
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Date of Birth</Form.Label>
        <Form.Control type="date" placeholder="" />
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email</Form.Label>
        <Form.Control type="email" placeholder="Enter email" />
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Phone Number</Form.Label>
        <Form.Control type="text" placeholder="Phone Number" />
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>NIK/Passport Number</Form.Label>
        <Form.Control type="text" placeholder="NIK/Passport Number" />
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Address</Form.Label>
        <Form.Control type="text" placeholder="Enter Address" />
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Special Need</Form.Label>
        <Form.Control as="textarea" placeholder="Special Need" />
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Code Promo</Form.Label>
        <Form.Control type="text" placeholder="Code Promo<" />
      </Form.Group>
      <Row>
        <Col>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Check In</Form.Label>
            <Form.Control type="date" placeholder="" />
          </Form.Group>
        </Col>
        <Col>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Check out</Form.Label>
            <Form.Control type="date" placeholder="" />
          </Form.Group>
        </Col>
      </Row>
      <Row>
        <Col>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Room Type</Form.Label>
            <Form.Control type="text" placeholder="Room Type" />
          </Form.Group>
        </Col>
        <Col>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Rooms</Form.Label>
            <Form.Control type="text" placeholder="Rooms" />
          </Form.Group>
        </Col>
        <Row>
          <Col sm="4">
            <Card>
              <Card.Header>
                <Form.Group className="mb-3" controlId="formBasicCheckbox">
                  <Form.Check type="checkbox" label="" />
                </Form.Group>
              </Card.Header>
              <Card.Body>
                <p
                  style={{
                    position: "absolute",
                    left: "50%",
                    top: "50%",
                    transform: "translate(-50%, -50%)",
                    fontSize: "20px",
                  }}
                >
                  QRIS
                </p>
              </Card.Body>
            </Card>
          </Col>
          <Col sm="4">
            <Card>
              <Card.Header>
                <Form.Group className="mb-3" controlId="formBasicCheckbox">
                  <Form.Check type="checkbox" label="" />
                </Form.Group>
              </Card.Header>
              <Card.Body>
                <p
                  style={{
                    position: "absolute",
                    left: "50%",
                    top: "50%",
                    transform: "translate(-50%, -50%)",
                    fontSize: "20px",
                  }}
                >
                  CASH
                </p>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Row>
      {/* <Button
    variant="outline-warning"
    style={{
      float: "right",
    }}
    onClick={handlekirimwa}
  >
    Bukti Wa
  </Button>{" "}
  <Button
    variant="outline-warning"
    style={{
      float: "right",
    }}
    onClick={handlekirimemail}
  >
    Bukti Email
  </Button>{" "} */}
      <Button
        variant="outline-warning"
        style={{
          float: "right",
        }}
        // onClick={handleOrder}
      >
        Order Now
      </Button>{" "}
    </Form>
  );
};

export default FormPayment;

import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const FormAddBank = () => {
  const initialState = {
    uuid_master_kamar: '',
    nama_kamar: '',
    type_kamar: '',
  }
  const [data, setData] = useState(initialState);
  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  const [msg, setMsg] = useState("");
  const navigate = useNavigate();

  const saveUser = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/masterkamar", data);
      navigate("/room");
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  };
  return (
    <div>
      <h1 className="title">Room</h1>
      <h2 className="subtitle">Add New Room</h2>
      <div className="card is-shadowless">
        <div className="card-content">
          <div className="content">
            <form onSubmit={saveUser}>
              <p className="has-text-centered">{msg}</p>
              <div className="field">
                <label className="label">ID Master Room</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="uuid_master_kamar"
                    value={data.uuid_master_kamar}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Room Type</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="type_kamar"
                    value={data.type_kamar}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Room Name</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="nama_kamar"
                    value={data.nama_kamar}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <button type="submit" className="button is-success">
                    Save
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormAddBank;

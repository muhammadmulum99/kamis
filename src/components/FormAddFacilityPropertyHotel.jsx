import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const FormAddBank = () => {
  const initialState = {
    uuid_properti_fasilitas_hotel: '',
    id_properti_hotel: '',
    id_master_fasilitas: '',
    status_properti_fasilitas_hotel: '',
    foto_fasilitas_hotel: '',
  }
  const [data, setData] = useState(initialState);
  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  const [msg, setMsg] = useState("");
  const navigate = useNavigate();

  const saveUser = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/propertifasilitashotel", data);
      navigate("/facilitypropertyhotel");
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  };
  const uploadFiles = (e) => {
    var f = e.target.files[0]; // FileList object
    var reader = new FileReader();
    // Closure to capture the file information.
    reader.onload = (function(theFile) {
      return function(e) {
        var binaryData = e.target.result;
        //Converting Binary Data to base 64
        var base64String = window.btoa(binaryData);
        //showing file converted to base64
        // document.getElementById('base64').value = base64String;
        setData({
          ...data,
          foto_fasilitas_hotel: base64String,
        })
        // alert('File converted to base64 successfuly!\nCheck in Textarea');
      };
    })(f);
    // Read in the image file as a data URL.
    reader.readAsBinaryString(f);
  }
  return (
    <div>
      <h1 className="title">Property Facility Hotel</h1>
      <h2 className="subtitle">Add New Property Facility Hotel</h2>
      <div className="card is-shadowless">
        <div className="card-content">
          <div className="content">
            <form onSubmit={saveUser}>
              <p className="has-text-centered">{msg}</p>
              <div className="field">
                <label className="label">ID Property Fasilitas Hotel</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="uuid_properti_fasilitas_hotel"
                    value={data.uuid_properti_fasilitas_hotel}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">ID Property Hotel</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="id_properti_hotel"
                    value={data.id_properti_hotel}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">ID Master Facility</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="id_master_fasilitas"
                    value={data.id_master_fasilitas}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Status</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="status_properti_fasilitas_hotel"
                    value={data.status_properti_fasilitas_hotel}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Photo</label>
                <div className="control">
                  <input
                    type="file"
                    className="input"
                    name="foto_fasilitas_hotel"
                    onChange={uploadFiles}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <button type="submit" className="button is-success">
                    Save
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormAddBank;

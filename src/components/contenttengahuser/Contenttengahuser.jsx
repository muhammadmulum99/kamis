import React from "react";
import "./contenttengahuser.css";
import { Container, Row, Col, Card } from "react-bootstrap";
import picdesti from "./benderaindo.jpg";

const Contenttengahuser = () => {
  return (
    <>
      <div className="main-content-tengah">
        <h3
          style={{
            marginTop: "20px",
            marginBottom: "20px",
            marginLeft: "20px",
          }}
        >
          Popular Destination
        </h3>
        <Container>
          <Row>
            <Col style={{ backgroundCWolor: "tomato" }}>
              <Card className="bg-dark text-white">
                <Card.Img src={picdesti} alt="Card image" />
                <Card.ImgOverlay>
                  <Card.Title
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignContent: "center",
                      position: "relative",
                      marginTop: "55px",
                      fontSize: "40px",
                      color: "black",
                    }}
                  >
                    Bandung
                  </Card.Title>
                  {/* <Card.Text>
                    This is a wider card with supporting text below as a natural
                    lead-in to additional content. This content is a little bit
                    longer.
                  </Card.Text> */}
                  {/* <Card.Text>Last updated 3 mins ago</Card.Text> */}
                </Card.ImgOverlay>
              </Card>
            </Col>
            <Col>
              {" "}
              <Card className="bg-dark text-white">
                <Card.Img src={picdesti} alt="Card image" />
                <Card.ImgOverlay>
                  <Card.Title
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignContent: "center",
                      position: "relative",
                      marginTop: "55px",
                      fontSize: "40px",
                      color: "black",
                    }}
                  >
                    Bandung
                  </Card.Title>
                  {/* <Card.Text>
                    This is a wider card with supporting text below as a natural
                    lead-in to additional content. This content is a little bit
                    longer.
                  </Card.Text> */}
                  {/* <Card.Text>Last updated 3 mins ago</Card.Text> */}
                </Card.ImgOverlay>
              </Card>
            </Col>
            <Col>
              {" "}
              <Card className="bg-dark text-white">
                <Card.Img src={picdesti} alt="Card image" />
                <Card.ImgOverlay>
                  <Card.Title
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignContent: "center",
                      position: "relative",
                      marginTop: "55px",
                      fontSize: "40px",
                      color: "black",
                    }}
                  >
                    Bandung
                  </Card.Title>
                  {/* <Card.Text>
                    This is a wider card with supporting text below as a natural
                    lead-in to additional content. This content is a little bit
                    longer.
                  </Card.Text> */}
                  {/* <Card.Text>Last updated 3 mins ago</Card.Text> */}
                </Card.ImgOverlay>
              </Card>
            </Col>
          </Row>
        </Container>

        <h3
          style={{
            marginTop: "20px",
            marginBottom: "20px",
            marginLeft: "20px",
          }}
        >
          Lates News
        </h3>
        <Container>
          <Row>
            <Col style={{ backgroundCWolor: "tomato" }}>
              <Card className="bg-dark text-white">
                <Card.Img src={picdesti} alt="Card image" />
                <Card.ImgOverlay>
                  <Card.Title
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignContent: "center",
                      position: "relative",
                      marginTop: "55px",
                      fontSize: "40px",
                      color: "black",
                    }}
                  >
                    Bandung
                  </Card.Title>
                  {/* <Card.Text>
                    This is a wider card with supporting text below as a natural
                    lead-in to additional content. This content is a little bit
                    longer.
                  </Card.Text> */}
                  {/* <Card.Text>Last updated 3 mins ago</Card.Text> */}
                </Card.ImgOverlay>
              </Card>
            </Col>
            <Col>
              {" "}
              <Card className="bg-dark text-white">
                <Card.Img src={picdesti} alt="Card image" />
                <Card.ImgOverlay>
                  <Card.Title
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignContent: "center",
                      position: "relative",
                      marginTop: "55px",
                      fontSize: "40px",
                      color: "black",
                    }}
                  >
                    Bandung
                  </Card.Title>
                  {/* <Card.Text>
                    This is a wider card with supporting text below as a natural
                    lead-in to additional content. This content is a little bit
                    longer.
                  </Card.Text> */}
                  {/* <Card.Text>Last updated 3 mins ago</Card.Text> */}
                </Card.ImgOverlay>
              </Card>
            </Col>
            <Col>
              {" "}
              <Card className="bg-dark text-white">
                <Card.Img src={picdesti} alt="Card image" />
                <Card.ImgOverlay>
                  <Card.Title
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignContent: "center",
                      position: "relative",
                      marginTop: "55px",
                      fontSize: "40px",
                      color: "black",
                    }}
                  >
                    Bandung
                  </Card.Title>
                  {/* <Card.Text>
                    This is a wider card with supporting text below as a natural
                    lead-in to additional content. This content is a little bit
                    longer.
                  </Card.Text> */}
                  {/* <Card.Text>Last updated 3 mins ago</Card.Text> */}
                </Card.ImgOverlay>
              </Card>
            </Col>
          </Row>
        </Container>

        <h3
          style={{
            marginTop: "20px",
            marginBottom: "20px",
            marginLeft: "20px",
          }}
        >
          Promos
        </h3>
        <Container>
          <Row>
            <Col style={{ backgroundCWolor: "tomato" }}>
              <Card className="bg-dark text-white">
                <Card.Img src={picdesti} alt="Card image" />
                <Card.ImgOverlay>
                  <Card.Title
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignContent: "center",
                      position: "relative",
                      marginTop: "55px",
                      fontSize: "40px",
                      color: "black",
                    }}
                  >
                    Bandung
                  </Card.Title>
                </Card.ImgOverlay>
              </Card>
            </Col>
            <Col>
              {" "}
              <Card className="bg-dark text-white">
                <Card.Img src={picdesti} alt="Card image" />
                <Card.ImgOverlay>
                  <Card.Title
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignContent: "center",
                      position: "relative",
                      marginTop: "55px",
                      fontSize: "40px",
                      color: "black",
                    }}
                  >
                    Bandung
                  </Card.Title>
                </Card.ImgOverlay>
              </Card>
            </Col>
            <Col>
              {" "}
              <Card className="bg-dark text-white">
                <Card.Img src={picdesti} alt="Card image" />
                <Card.ImgOverlay>
                  <Card.Title
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignContent: "center",
                      position: "relative",
                      marginTop: "55px",
                      fontSize: "40px",
                      color: "black",
                    }}
                  >
                    Bandung
                  </Card.Title>
                </Card.ImgOverlay>
              </Card>
            </Col>
          </Row>
        </Container>

        <h3
          style={{
            marginTop: "20px",
            marginBottom: "20px",
            marginLeft: "20px",
          }}
        >
          Recently Viewed
        </h3>
        <Container>
          <Row>
            <Col style={{ backgroundCWolor: "tomato" }}>
              <Card className="bg-dark text-white">
                <Card.Img src={picdesti} alt="Card image" />
                <Card.ImgOverlay>
                  <Card.Title
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignContent: "center",
                      position: "relative",
                      marginTop: "55px",
                      fontSize: "40px",
                      color: "black",
                    }}
                  >
                    Bandung
                  </Card.Title>
                </Card.ImgOverlay>
              </Card>
            </Col>
            <Col>
              {" "}
              <Card className="bg-dark text-white">
                <Card.Img src={picdesti} alt="Card image" />
                <Card.ImgOverlay>
                  <Card.Title
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignContent: "center",
                      position: "relative",
                      marginTop: "55px",
                      fontSize: "40px",
                      color: "black",
                    }}
                  >
                    Bandung
                  </Card.Title>
                </Card.ImgOverlay>
              </Card>
            </Col>
            <Col>
              {" "}
              <Card className="bg-dark text-white">
                <Card.Img src={picdesti} alt="Card image" />
                <Card.ImgOverlay>
                  <Card.Title
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignContent: "center",
                      position: "relative",
                      marginTop: "55px",
                      fontSize: "40px",
                      color: "black",
                    }}
                  >
                    Bandung
                  </Card.Title>
                </Card.ImgOverlay>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default Contenttengahuser;

import { BrowserRouter, Routes, Route } from "react-router-dom";
import Dashboard from "./pages/Dashboard";
import HomeUser from "./pages/HomeUser";
import TransactionUser from "./pages/TransactionUser";
import PaymentUser from "./pages/PaymentUser";
import Wishlist from "./pages/Wishlist";
import Hotels from "./pages/Hotels";
import Orders from "./pages/Orders";
import Login from "./components/Login";
import Users from "./pages/Users";
import AddUser from "./pages/AddUser";
import EditUser from "./pages/EditUser";
import Bank from "./pages/Bank";
import AddBank from "./pages/AddBank";
import EditBank from "./pages/EditBank";
import Products from "./pages/Products";
import AddProduct from "./pages/AddProduct";
import EditProduct from "./pages/EditProduct";

import Hotel from "./pages/Hotel";
import AddHotel from "./pages/AddHotel";
import EditHotel from "./pages/EditHotel";
import HotelCategory from "./pages/HotelCategory";
import AddHotelCategory from "./pages/AddHotelCategory";
import EditHotelCategory from "./pages/EditHotelCategory";
import HotelProperty from "./pages/HotelProperty";
import AddHotelProperty from "./pages/AddHotelProperty";
import EditHotelProperty from "./pages/EditHotelProperty";

import Room from "./pages/Room";
import AddRoom from "./pages/AddRoom";
import EditRoom from "./pages/EditRoom";
import RoomProperty from "./pages/RoomProperty";
import AddRoomProperty from "./pages/AddRoomProperty";
import EditRoomProperty from "./pages/EditRoomProperty";
import RoomPropertyDetail from "./pages/RoomPropertyDetail";
import AddRoomPropertyDetail from "./pages/AddRoomPropertyDetail";
import EditRoomPropertyDetail from "./pages/EditRoomPropertyDetail";

import Facility from "./pages/Facility";
import AddFacility from "./pages/AddFacility";
import EditFacility from "./pages/EditFacility";
import FacilityPropertyExtra from "./pages/FacilityPropertyExtra";
import AddFacilityPropertyExtra from "./pages/AddFacilityPropertyExtra";
import EditFacilityPropertyExtra from "./pages/EditFacilityPropertyExtra";
import FacilityPropertyHotel from "./pages/FacilityPropertyHotel";
import AddFacilityPropertyHotel from "./pages/AddFacilityPropertyHotel";
import EditFacilityPropertyHotel from "./pages/EditFacilityPropertyHotel";
import FacilityPropertyRoom from "./pages/FacilityPropertyRoom";
import AddFacilityPropertyRoom from "./pages/AddFacilityPropertyRoom";
import EditFacilityPropertyRoom from "./pages/EditFacilityPropertyRoom";
import Detailhotel from "./pages/Detailhotel";

import Kasir from "./pages/Kasir";
import Checkin from "./pages/Checkin";
import Checkout from "./pages/Checkout";
import RoomChecker from "./pages/RoomChecker";
import Orderroom from "./pages/Orderroom";
import Paymentkasir from "./pages/Paymentkasir";

import Profilemanager from "./pages/manager/Profilemanager";
import Facilitesmanager from "./pages/manager/Facilitesmanager";
import Financialmanager from "./pages/manager/Financialmanager";
import Reportmanager from "./pages/manager/Reportmanager";
import Roommanager from "./pages/manager/Roommanager";
import Settingmanager from "./pages/manager/Settingmanager";
import Staffmanager from "./pages/manager/Staffmanager";

import Profile from "./pages/owner/Profile";
import Financial from "./pages/owner/Financial";
import Setting from "./pages/owner/Setting";
import Properti from "./pages/owner/Properti";
import Report from "./pages/owner/Report";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomeUser />} />
          <Route path="/login" element={<Login />} />
          <Route path="/wishlist" element={<Wishlist />} />
          <Route path="/orders" element={<Orders />} />
          <Route path="/hotels" element={<Hotels />} />
          <Route path="/hotels/:id" element={<Detailhotel />} />
          <Route path="/transaction" element={<TransactionUser />} />
          <Route path="/payment" element={<PaymentUser />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/bank" element={<Bank />} />
          <Route path="/bank/add" element={<AddBank />} />
          <Route path="/bank/edit/:id" element={<EditBank />} />
          <Route path="/fo-checkin" element={<Checkin />}/>
          <Route path="/fo-checkout" element={<Checkout />}/>
          <Route path="/fo-orderroom" element={<Orderroom />}/>
          <Route path="/fo-roomchecker" element={<RoomChecker />}/>
          <Route path="/fo-kasir" element={<Kasir />}/>
          <Route path="/fo-payment" element={<Paymentkasir />}/>
          <Route path="/users" element={<Users />} />
          <Route path="/users/add" element={<AddUser />} />
          <Route path="/users/edit/:id" element={<EditUser />} />
          <Route path="/products" element={<Products />} />
          <Route path="/products/add" element={<AddProduct />} />
          <Route path="/products/edit/:id" element={<EditProduct />} />
          <Route path="/hotel" element={<Hotel />} />
          <Route path="/hotel/add" element={<AddHotel />} />
          <Route path="/hotel/edit/:id" element={<EditHotel />} />
          <Route path="/hotelcategory" element={<HotelCategory />} />
          <Route path="/hotelcategory/add" element={<AddHotelCategory />} />
          <Route path="/hotelcategory/edit/:id" element={<EditHotelCategory />} />
          <Route path="/hotelproperty" element={<HotelProperty />} />
          <Route path="/hotelproperty/add" element={<AddHotelProperty />} />
          <Route path="/hotelproperty/edit/:id" element={<EditHotelProperty />} />
          <Route path="/room" element={<Room />} />
          <Route path="/room/add" element={<AddRoom />} />
          <Route path="/room/edit/:id" element={<EditRoom />} />
          <Route path="/roomproperty" element={<RoomProperty />} />
          <Route path="/roomproperty/add" element={<AddRoomProperty />} />
          <Route path="/roomproperty/edit/:id" element={<EditRoomProperty />} />
          <Route path="/roompropertydetail" element={<RoomPropertyDetail />} />
          <Route path="/roompropertydetail/add" element={<AddRoomPropertyDetail />} />
          <Route path="/roompropertydetail/edit/:id" element={<EditRoomPropertyDetail />} />
          <Route path="/facility" element={<Facility />} />
          <Route path="/facility/add" element={<AddFacility />} />
          <Route path="/facility/edit/:id" element={<EditFacility />} />
          <Route path="/facilitypropertyextra" element={<FacilityPropertyExtra />} />
          <Route path="/facilitypropertyextra/add" element={<AddFacilityPropertyExtra />} />
          <Route path="/facilitypropertyextra/edit/:id" element={<EditFacilityPropertyExtra />} />
          <Route path="/facilitypropertyhotel" element={<FacilityPropertyHotel />} />
          <Route path="/facilitypropertyhotel/add" element={<AddFacilityPropertyHotel />} />
          <Route path="/facilitypropertyhotel/edit/:id" element={<EditFacilityPropertyHotel />} />
          <Route path="/facilitypropertyroom" element={<FacilityPropertyRoom />} />
          <Route path="/facilitypropertyroom/add" element={<AddFacilityPropertyRoom />} />
          <Route path="/facilitypropertyroom/edit/:id" element={<EditFacilityPropertyRoom />} />
          <Route
          path="/manager-profile"
          element={<Profilemanager />}
          />
          <Route path="/manager-staff" element={<Staffmanager />}></Route>
          <Route
            path="/manager-facilities"
            element={<Facilitesmanager />}
          />
          <Route path="/manager-rooms" element={<Roommanager />}/>
          <Route
            path="/manager-financial"
            element={<Financialmanager />}
          />
          <Route path="/manager-report" element={<Reportmanager />}/>
          <Route
            path="/manager-settings"
            element={<Settingmanager />}
          />
          <Route
            path="/manager-profile"
            element={<Profilemanager />}
          />
          <Route path="/owner-profile" element={<Profile />}/>
          <Route path="/owner-financial" element={<Financial />}/>
          <Route path="/owner-setting" element={<Setting />}/>
          <Route path="/owner-report" element={<Report />}/>
          <Route path="/owner-properti" element={<Properti />}/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
